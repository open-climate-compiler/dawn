#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/kparallel_solver_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/kparallel_solver_gen.cu"

using namespace dawn;
TEST(kparallel_solver_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(halo::value, halo::value, halo::value, halo::value, 0, 0);

  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  storage_t d_naive(meta_data, "d"), d_mlir(meta_data, "d_mlir"), a(meta_data, "a"), b(meta_data, "b"),
      c_mlir(meta_data, "c_mlir"), c_naive(meta_data, "c_naive");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* a_ptr = new double[total_size];
  double* b_ptr = new double[total_size];
  double* c_mlir_ptr = new double[total_size];
  double* d_mlir_ptr = new double[total_size];

  auto a_view = make_host_view(a);
  auto b_view = make_host_view(b);
  auto c_mlir_view = make_host_view(c_mlir);
  auto d_mlir_view = make_host_view(d_mlir);

  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, d_naive);
  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, d_mlir);
  verif.fillMath(7.4, 2.0, 1.5, 1.3, 2.1, 3.0, a);
  verif.fillMath(8.0, 2.0, 1.4, 1.2, 2.3, 3.0, b);
  verif.fillMath(7.8, 2.0, 1.1, 1.7, 1.9, 4.1, c_mlir);
  verif.fillMath(7.8, 2.0, 1.1, 1.7, 1.9, 4.1, c_naive);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        a_ptr[offset] = a_view(i, j, k);
        b_ptr[offset] = b_view(i, j, k);
        c_mlir_ptr[offset] = c_mlir_view(i, j, k);
        d_mlir_ptr[offset] = d_mlir_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::kparallel_solver kparallel_solver_naive(dom);

  kparallel_solver_naive.run(d_naive, a, b, c_naive);
  _kparallel_solver_mlir(isize, jsize, ksize, halo::value, d_mlir_ptr, a_ptr, b_ptr, c_mlir_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        a_view(i, j, k) = a_ptr[offset];
        b_view(i, j, k) = b_ptr[offset];
        c_mlir_view(i, j, k) = c_mlir_ptr[offset];
        d_mlir_view(i, j, k) = d_mlir_ptr[offset];
      }
    }
  }

  delete[] a_ptr;
  delete[] b_ptr;
  delete[] c_mlir_ptr;
  delete[] d_mlir_ptr;

  ASSERT_TRUE(verif.verify(d_mlir, d_naive));
}
