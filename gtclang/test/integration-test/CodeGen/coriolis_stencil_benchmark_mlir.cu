#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/coriolis_stencil_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/coriolis_stencil_gen.cu"

using namespace dawn;
TEST(coriolis_stencil_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(halo::value, halo::value, halo::value, halo::value, 0, 0);
  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  storage_t u_tens_mlir(meta_data, "u_tens_mlir"), u_tens_cxxnaive(meta_data, "u_tens_cxxnaive");
  storage_t v_tens_mlir(meta_data, "v_tens_mlir"), v_tens_cxxnaive(meta_data, "v_tens_cxxnaive");
  storage_t u_nnow(meta_data, "u_nnow"), v_nnow(meta_data, "v_nnow"), fc(meta_data, "fc");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* u_tens_mlir_ptr = new double[total_size];
  double* u_nnow_ptr = new double[total_size];
  double* v_tens_mlir_ptr = new double[total_size];
  double* v_nnow_ptr = new double[total_size];
  double* fc_ptr = new double[total_size];

  auto u_tens_mlir_view = make_host_view(u_tens_mlir);
  auto u_nnow_view = make_host_view(u_nnow);
  auto v_tens_mlir_view = make_host_view(v_tens_mlir);
  auto v_nnow_view = make_host_view(v_nnow);
  auto fc_view = make_host_view(fc);

  verif.fill(-1.0, u_tens_mlir, v_tens_mlir, u_tens_cxxnaive, v_tens_cxxnaive);
  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, u_nnow);
  verif.fillMath(5.0, 1.2, 1.3, 1.7, 2.2, 3.5, v_nnow);
  verif.fillMath(2.0, 1.3, 1.4, 1.6, 2.1, 3.0, fc);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_tens_mlir_ptr[offset] = u_tens_mlir_view(i, j, k);
        u_nnow_ptr[offset] = u_nnow_view(i, j, k);
        v_tens_mlir_ptr[offset] = v_tens_mlir_view(i, j, k);
        v_nnow_ptr[offset] = v_nnow_view(i, j, k);
        fc_ptr[offset] = fc_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::coriolis_stencil coriolis_cxxnaive(dom);

  coriolis_cxxnaive.run(u_tens_cxxnaive, u_nnow, v_tens_cxxnaive,
                        v_nnow, fc);
  _coriolis_stencil_mlir(isize, jsize, ksize, halo::value, u_tens_mlir_ptr, u_nnow_ptr, v_tens_mlir_ptr, v_nnow_ptr, fc_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_tens_mlir_view(i, j, k) = u_tens_mlir_ptr[offset];
        u_nnow_view(i, j, k) = u_nnow_ptr[offset];
        v_tens_mlir_view(i, j, k) = v_tens_mlir_ptr[offset];
        v_nnow_view(i, j, k) = v_nnow_ptr[offset];
        fc_view(i, j, k) = fc_ptr[offset];
      }
    }
  }

  delete[] u_tens_mlir_ptr;
  delete[] u_nnow_ptr;
  delete[] v_tens_mlir_ptr;
  delete[] v_nnow_ptr;
  delete[] fc_ptr;

  ASSERT_TRUE(verif.verify(u_tens_mlir, u_tens_cxxnaive));
  ASSERT_TRUE(verif.verify(v_tens_mlir, v_tens_cxxnaive));
}
