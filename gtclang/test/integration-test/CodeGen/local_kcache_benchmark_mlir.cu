#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/local_kcache_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/local_kcache_gen.cu"

using namespace dawn;
TEST(local_kcache_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(halo::value, halo::value, halo::value, halo::value, 0, 0);

  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  storage_t a_mlir(meta_data, "a_mlir"), a_naive(meta_data, "a_naive"), b_naive(meta_data, "b"), b_mlir(meta_data, "b_mlir");
  storage_t c_mlir(meta_data, "c_mlir"), c_naive(meta_data, "c_naive"), d_naive(meta_data, "d"), d_mlir(meta_data, "d_mlir");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* a_mlir_ptr = new double[total_size];
  double* b_mlir_ptr = new double[total_size];
  double* c_mlir_ptr = new double[total_size];
  double* d_mlir_ptr = new double[total_size];

  auto a_mlir_view = make_host_view(a_mlir);
  auto b_mlir_view = make_host_view(b_mlir);
  auto c_mlir_view = make_host_view(c_mlir);
  auto d_mlir_view = make_host_view(d_mlir);

  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.2, a_naive);
  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.2, a_mlir);
  verif.fillMath(5.8, 1.0, 1.7, 1.7, 1.9, 4.1, b_naive);
  verif.fillMath(5.8, 1.0, 1.7, 1.7, 1.9, 4.1, b_mlir);
  verif.fillMath(7.8, 2.0, 1.1, 1.7, 1.9, 4.1, c_naive);
  verif.fillMath(7.8, 2.0, 1.1, 1.7, 1.9, 4.1, c_mlir);
  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, d_naive);
  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, d_mlir);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        a_mlir_ptr[offset] = a_mlir_view(i, j, k);
        b_mlir_ptr[offset] = b_mlir_view(i, j, k);
        c_mlir_ptr[offset] = c_mlir_view(i, j, k);
        d_mlir_ptr[offset] = d_mlir_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::local_kcache local_kcache_naive(dom);

  local_kcache_naive.run(a_naive, b_naive, c_naive, d_naive);
  _local_kcache_mlir(isize, jsize, ksize, halo::value, a_mlir_ptr, b_mlir_ptr, c_mlir_ptr, d_mlir_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        a_mlir_view(i, j, k) = a_mlir_ptr[offset];
        b_mlir_view(i, j, k) = b_mlir_ptr[offset];
        c_mlir_view(i, j, k) = c_mlir_ptr[offset];
        d_mlir_view(i, j, k) = d_mlir_ptr[offset];
      }
    }
  }

  delete[] a_mlir_ptr;
  delete[] b_mlir_ptr;
  delete[] c_mlir_ptr;
  delete[] d_mlir_ptr;

  ASSERT_TRUE(verif.verify(a_mlir, a_naive));
  ASSERT_TRUE(verif.verify(b_mlir, b_naive));
  ASSERT_TRUE(verif.verify(c_mlir, c_naive));
  ASSERT_TRUE(verif.verify(d_mlir, d_naive));
}
