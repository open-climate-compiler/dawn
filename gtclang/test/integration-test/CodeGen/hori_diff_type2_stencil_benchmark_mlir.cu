#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/hori_diff_type2_stencil_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/hori_diff_type2_stencil_gen.cu"

using namespace dawn;
TEST(hori_diff_type2_stencil_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(GRIDTOOLS_CLANG_HALO_EXTEND, GRIDTOOLS_CLANG_HALO_EXTEND,
                GRIDTOOLS_CLANG_HALO_EXTEND, GRIDTOOLS_CLANG_HALO_EXTEND, 0, 0);

  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  meta_data_j_t meta_data_j(1, dom.jsize(), 1);

  storage_t u(meta_data, "u"), hdmask(meta_data, "hdmask"), out_naive(meta_data, "out_naive"),
      u_out_mlir(meta_data, "u"), u_out_naive(meta_data, "u");

  storage_j_t crlato(meta_data_j, "crlato"), crlatu(meta_data_j, "crlatu");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* u_out_mlir_ptr = new double[total_size];
  double* u_ptr = new double[total_size];
  double* crlato_ptr = new double[total_size];
  double* crlatu_ptr = new double[total_size];
  double* hdmask_ptr = new double[total_size];

  auto u_out_mlir_view = make_host_view(u_out_mlir);
  auto u_view = make_host_view(u);
  auto crlato_view = make_host_view(crlato);
  auto crlatu_view = make_host_view(crlatu);
  auto hdmask_view = make_host_view(hdmask);

  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, u);
  verif.fillMath(5.0, 2.2, 1.7, 1.9, 2.0, 4.0, hdmask);
  verif.fillMath(6.5, 1.2, 1.7, 0.9, 2.1, 2.0, crlato);
  verif.fillMath(5.0, 2.2, 1.7, 0.9, 2.0, 1.0, crlatu);

  verif.fill(-1.0, u_out_mlir, u_out_naive);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_out_mlir_ptr[offset] = u_out_mlir_view(i, j, k);
        u_ptr[offset] = u_view(i, j, k);
        crlato_ptr[offset] = crlato_view(i, j, k);
        crlatu_ptr[offset] = crlatu_view(i, j, k);
        hdmask_ptr[offset] = hdmask_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::hori_diff_type2_stencil hd_naive(dom);

  hd_naive.run(u_out_naive, u, crlato, crlatu, hdmask);
  _hori_diff_type2_stencil_mlir(isize, jsize, ksize, halo::value, u_out_mlir_ptr, u_ptr, crlato_ptr, crlatu_ptr, hdmask_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_out_mlir_view(i, j, k) = u_out_mlir_ptr[offset];
        u_view(i, j, k) = u_ptr[offset];
        crlato_view(i, j, k) = crlato_ptr[offset];
        crlatu_view(i, j, k) = crlatu_ptr[offset];
        hdmask_view(i, j, k) = hdmask_ptr[offset];
      }
    }
  }

  delete[] u_out_mlir_ptr;
  delete[] u_ptr;
  delete[] crlato_ptr;
  delete[] crlatu_ptr;
  delete[] hdmask_ptr;

  ASSERT_TRUE(verif.verify(u_out_mlir, u_out_naive));
}
