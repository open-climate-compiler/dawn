#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/hori_diff_stencil_01_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/hori_diff_stencil_01_gen.cu"

using namespace dawn;
TEST(hori_diff_stencil_01_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(halo::value, halo::value, halo::value, halo::value, 0, 0);

  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  storage_t u(meta_data, "u"), out_mlir(meta_data, "out_mlir"), out_naive(meta_data, "out_naive");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* u_ptr = new double[total_size];
  double* out_mlir_ptr = new double[total_size];

  auto u_view = make_host_view(u);
  auto out_mlir_view = make_host_view(out_mlir);

  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, u);
  verif.fill(-1.0, out_mlir, out_naive);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_ptr[offset] = u_view(i, j, k);
        out_mlir_ptr[offset] = out_mlir_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::hori_diff_stencil hori_diff_naive(dom);

  hori_diff_naive.run(u, out_naive);
  _hori_diff_stencil_mlir(isize, jsize, ksize, halo::value, u_ptr, out_mlir_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_view(i, j, k) = u_ptr[offset];
        out_mlir_view(i, j, k) = out_mlir_ptr[offset];
      }
    }
  }

  delete[] out_mlir_ptr;
  delete[] u_ptr;

  ASSERT_TRUE(verif.verify(out_mlir, out_naive));
}
