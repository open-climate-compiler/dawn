#define GRIDTOOLS_CLANG_GENERATED 1
#define GRIDTOOLS_CLANG_CUDA 1
#define GRIDTOOLS_CLANG_HALO_EXTEND 3
#define GT_VECTOR_LIMIT_SIZE 30

#undef FUSION_MAX_VECTOR_SIZE
#undef FUSION_MAX_MAP_SIZE
#define FUSION_MAX_VECTOR_SIZE GT_VECTOR_LIMIT_SIZE
#define FUSION_MAX_MAP_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE FUSION_MAX_VECTOR_SIZE
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS

#include <gtest/gtest.h>
#include "test/integration-test/CodeGen/Macros.hpp"
#include "gridtools/clang/verify.hpp"
#include "test/integration-test/CodeGen/Options.hpp"
#include "test/integration-test/CodeGen/generated/hd_smagorinsky_c++-naive.cpp"
#include "test/integration-test/CodeGen/generated/mlir/hd_smagorinsky_gen.cu"

using namespace dawn;
TEST(hd_smagorinsky_mlir, test) {
  domain dom(Options::getInstance().m_size[0], Options::getInstance().m_size[1],
             Options::getInstance().m_size[2]);
  dom.set_halos(halo::value, halo::value, halo::value, halo::value, 0, 0);

  verifier verif(dom);

  meta_data_t meta_data(dom.isize(), dom.jsize(), dom.ksize() + 1);
  meta_data_j_t meta_data_j(1, dom.jsize(), 1);
  meta_data_scalar_t meta_data_scalar(1, 1, 1);

  // Output fields
  storage_t u_out_mlir(meta_data, "v_out");
  storage_t v_out_mlir(meta_data, "u_out");
  storage_t u_out_naive(meta_data, "v_out");
  storage_t v_out_naive(meta_data, "u_out");

  // Input fields
  storage_t u_in(meta_data, "u_in");
  storage_t v_in(meta_data, "v_in");
  storage_t hdmaskvel(meta_data, "hdmaskvel");
  storage_j_t crlavo(meta_data_j, "crlavo");
  storage_j_t crlavu(meta_data_j, "crlavu");
  storage_j_t crlato(meta_data_j, "crlato");
  storage_j_t crlatu(meta_data_j, "crlatu");
  storage_j_t acrlat0(meta_data_j, "acrlat0");

  // Scalar fields
  storage_t eddlon(meta_data, "eddlon");
  storage_t eddlat(meta_data, "eddlat");
  storage_t tau_smag(meta_data, "tau_smag");
  storage_t weight_smag(meta_data, "weight_smag");

  unsigned int isize = (dom.isize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int jsize = (dom.jsize() + 2 * GRIDTOOLS_CLANG_HALO_EXTEND);
  unsigned int ksize = dom.ksize();
  unsigned int total_size = isize * jsize * ksize;
  double* u_out_mlir_ptr = new double[total_size];
  double* v_out_mlir_ptr = new double[total_size];
  double* u_in_ptr = new double[total_size];
  double* v_in_ptr = new double[total_size];
  double* hdmaskvel_ptr = new double[total_size];
  double* crlavo_ptr = new double[total_size];
  double* crlavu_ptr = new double[total_size];
  double* crlato_ptr = new double[total_size];
  double* crlatu_ptr = new double[total_size];
  double* acrlat0_ptr = new double[total_size];
  double* eddlon_ptr = new double[total_size];
  double* eddlat_ptr = new double[total_size];
  double* tau_smag_ptr = new double[total_size];
  double* weight_smag_ptr = new double[total_size];

  auto u_out_mlir_view = make_host_view(u_out_mlir);
  auto v_out_mlir_view = make_host_view(v_out_mlir);
  auto u_in_view = make_host_view(u_in);
  auto v_in_view = make_host_view(v_in);
  auto hdmaskvel_view = make_host_view(hdmaskvel);
  auto crlavo_view = make_host_view(crlavo);
  auto crlavu_view = make_host_view(crlavu);
  auto crlato_view = make_host_view(crlato);
  auto crlatu_view = make_host_view(crlatu);
  auto acrlat0_view = make_host_view(acrlat0);
  auto eddlon_view = make_host_view(eddlon);
  auto eddlat_view = make_host_view(eddlat);
  auto tau_smag_view = make_host_view(tau_smag);
  auto weight_smag_view = make_host_view(weight_smag);

  verif.fillMath(8.0, 2.0, 1.5, 1.5, 2.0, 4.0, u_in);
  verif.fillMath(6.0, 1.0, 0.9, 1.1, 2.0, 4.0, v_in);
  verif.fillMath(5.0, 2.2, 1.7, 1.9, 2.0, 4.0, hdmaskvel);
  verif.fillMath(6.5, 1.2, 1.7, 1.9, 2.1, 2.0, crlavo);
  verif.fillMath(5.0, 2.2, 1.7, 1.9, 2.0, 1.0, crlavu);
  verif.fillMath(6.5, 1.2, 1.7, 0.9, 2.1, 2.0, crlato);
  verif.fillMath(5.0, 2.2, 1.7, 0.9, 2.0, 1.0, crlatu);
  verif.fillMath(6.5, 1.2, 1.2, 1.2, 2.2, 2.2, acrlat0);

  verif.fill(-1.0, u_out_mlir, v_out_mlir, u_out_naive, v_out_naive);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_out_mlir_ptr[offset] = u_out_mlir_view(i, j, k);
        v_out_mlir_ptr[offset] = v_out_mlir_view(i, j, k);
        u_in_ptr[offset] = u_in_view(i, j, k);
        v_in_ptr[offset] = v_in_view(i, j, k);
        hdmaskvel_ptr[offset] = hdmaskvel_view(i, j, k);
        crlavo_ptr[offset] = crlavo_view(i, j, k);
        crlavu_ptr[offset] = crlavu_view(i, j, k);
        crlato_ptr[offset] = crlato_view(i, j, k);
        crlatu_ptr[offset] = crlatu_view(i, j, k);
        acrlat0_ptr[offset] = acrlat0_view(i, j, k);
        eddlon_ptr[offset] = eddlon_view(i, j, k);
        eddlat_ptr[offset] = eddlat_view(i, j, k);
        tau_smag_ptr[offset] = tau_smag_view(i, j, k);
        weight_smag_ptr[offset] = weight_smag_view(i, j, k);
      }
    }
  }

  dawn_generated::cxxnaive::hd_smagorinsky_stencil hd_smagorinsky_naive(dom);

  hd_smagorinsky_naive.run(u_out_naive, v_out_naive, u_in, v_in, hdmaskvel, crlavo, crlavu, crlato, crlatu, acrlat0,
                           eddlon, eddlat, tau_smag, weight_smag);
  _hd_smagorinsky_stencil_mlir(isize, jsize, ksize, halo::value, u_out_mlir_ptr, v_out_mlir_ptr, u_in_ptr, v_in_ptr, hdmaskvel_ptr,
                               crlavo_ptr, crlavu_ptr, crlato_ptr, crlatu_ptr, acrlat0_ptr, eddlon_ptr,
                               eddlat_ptr, tau_smag_ptr, weight_smag_ptr);

  for(int k = 0; k < dom.ksize(); k++) {
    for(int j = 0; j < dom.jsize(); j++) {
      for(int i = 0; i < dom.isize(); i++) {
        int ii = i + GRIDTOOLS_CLANG_HALO_EXTEND;
        int jj = j + GRIDTOOLS_CLANG_HALO_EXTEND;
        unsigned int offset = jsize * isize * k + isize * jj + ii;
        u_out_mlir_view(i, j, k) = u_out_mlir_ptr[offset];
        v_out_mlir_view(i, j, k) = v_out_mlir_ptr[offset];
        u_in_view(i, j, k) = u_in_ptr[offset];
        v_in_view(i, j, k) = v_in_ptr[offset];
        hdmaskvel_view(i, j, k) = hdmaskvel_ptr[offset];
        crlavo_view(i, j, k) = crlavo_ptr[offset];
        crlavu_view(i, j, k) = crlavu_ptr[offset];
        crlato_view(i, j, k) = crlato_ptr[offset];
        crlatu_view(i, j, k) = crlatu_ptr[offset];
        acrlat0_view(i, j, k) = acrlat0_ptr[offset];
        eddlon_view(i, j, k) = eddlon_ptr[offset];
        eddlat_view(i, j, k) = eddlat_ptr[offset];
        tau_smag_view(i, j, k) = tau_smag_ptr[offset];
        weight_smag_view(i, j, k) = weight_smag_ptr[offset];
      }
    }
  }

  delete[] u_out_mlir_ptr;
  delete[] v_out_mlir_ptr;
  delete[] u_in_ptr;
  delete[] v_in_ptr;
  delete[] hdmaskvel_ptr;
  delete[] crlavo_ptr;
  delete[] crlavu_ptr;
  delete[] crlato_ptr;
  delete[] crlatu_ptr;
  delete[] acrlat0_ptr;
  delete[] eddlon_ptr;
  delete[] eddlat_ptr;
  delete[] tau_smag_ptr;
  delete[] weight_smag_ptr;

  ASSERT_TRUE(verif.verify(u_out_mlir, u_out_naive));
  ASSERT_TRUE(verif.verify(v_out_mlir, v_out_naive));
}
