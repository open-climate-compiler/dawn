//===--------------------------------------------------------------------------------*- C++ -*-===//
//                          _
//                         | |
//                       __| | __ ___      ___ ___
//                      / _` |/ _` \ \ /\ / / '_  |
//                     | (_| | (_| |\ V  V /| | | |
//                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
//
//
//  This file is distributed under the MIT License (MIT).
//  See LICENSE.txt for details.
//
//===------------------------------------------------------------------------------------------===//
#include <dawn-c/Compiler.h>
#include <dawn-c/Options.h>
#include <dawn/CodeGen/TranslationUnit.h>
#include <dawn-c/util/TranslationUnitWrapper.h>
#include <fstream>
#include <iostream>
#include <sstream>

int main(int argc, char* argv[]) {
  if(argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <file>" << std::endl;
    return 1;
  }

  std::ifstream inputFile(argv[1]);
  if(!inputFile.is_open())
    return 1;

  std::stringstream ss;
  ss << inputFile.rdbuf();

  auto options = dawnOptionsCreate();
  auto entry = dawnOptionsEntryCreateInteger(1);
  dawnOptionsSet(options, "SerializeIIR", entry);
  auto formatEntry = dawnOptionsEntryCreateString("mlir");
  dawnOptionsSet(options, "IIRFormat", formatEntry);

  auto str = ss.str();

  auto tu = dawn::util::toConstTranslationUnit(dawnCompile(str.c_str(), str.length(), options));

  std::ostringstream os;
  for(auto const& macroDefine : tu->getPPDefines())
    os << macroDefine << "\n";

  os << tu->getGlobals();
  for(auto const& s : tu->getStencils())
    os << s.second;
  std::cout << os.str();

  return 0;
}
