//===--------------------------------------------------------------------------------*- C++ -*-===//
//                          _
//                         | |
//                       __| | __ ___      ___ ___
//                      / _` |/ _` \ \ /\ / / '_  |
//                     | (_| | (_| |\ V  V /| | | |
//                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
//
//
//  This file is distributed under the MIT License (MIT).
//  See LICENSE.txt for details.
//
//===------------------------------------------------------------------------------------------===//
#include <dawn-c/Compiler.h>
#include <dawn-c/Options.h>
#include <dawn/CodeGen/TranslationUnit.h>
#include <dawn-c/util/TranslationUnitWrapper.h>
#include <fstream>
#include <iostream>
#include <sstream>

int main(int argc, char* argv[]) {
  if(argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <file>" << std::endl;
    return 1;
  }

  auto options = dawnOptionsCreate();
  auto entry = dawnOptionsEntryCreateString(argv[1]);
  dawnOptionsSet(options, "DeserializeIIR", entry);
  auto formatEntry = dawnOptionsEntryCreateString("mlir");
  dawnOptionsSet(options, "IIRFormat", formatEntry);

  auto tu = dawn::util::toConstTranslationUnit(dawnCompile("", 0, options));

  std::ostringstream ss;
  for(auto const& macroDefine : tu->getPPDefines())
    ss << macroDefine << "\n";

  ss << tu->getGlobals();
  for(auto const& s : tu->getStencils())
    ss << s.second;
  std::cout << ss.str();

  return 0;
}
