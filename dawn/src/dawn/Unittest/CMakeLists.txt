##===------------------------------------------------------------------------------*- CMake -*-===##
##                          _                      
##                         | |                     
##                       __| | __ ___      ___ ___  
##                      / _` |/ _` \ \ /\ / / '_  | 
##                     | (_| | (_| |\ V  V /| | | |
##                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
##
##
##  This file is distributed under the MIT License (MIT). 
##  See LICENSE.txt for details.
##
##===------------------------------------------------------------------------------------------===##

yoda_add_library(
  NAME DawnUnittest
  SOURCES IIRBuilder.h
          IIRBuilder.cpp
          UnittestLogger.cpp
          UnittestLogger.h
  ARCHIVE
  OBJECT #TODO: we don't want it but it is needed for some gtclang tests
  DEPENDS DawnIIRStatic DawnOptimizerStatic
)
