//===--------------------------------------------------------------------------------*- C++ -*-===//
//                          _
//                         | |
//                       __| | __ ___      ___ ___
//                      / _` |/ _` \ \ /\ / / '_  |
//                     | (_| | (_| |\ V  V /| | | |
//                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
//
//
//  This file is distributed under the MIT License (MIT).
//  See LICENSE.txt for details.
//
//===----------------------------------------TypeKind--------------------------------------------------===//
#include "dawn/Serialization/IIRSerializer.h"
#include "dawn/AST/ASTStmt.h"
#include "dawn/IIR/ASTStmt.h"
#include "dawn/IIR/ASTVisitor.h"
#include "dawn/IIR/IIR/IIR.pb.h"
#include "dawn/IIR/IIRNodeIterator.h"
#include "dawn/IIR/MultiStage.h"
#include "dawn/IIR/StencilInstantiation.h"
#include "dawn/SIR/SIR.h"
#include "dawn/Serialization/ASTSerializer.h"
#include "dawn/Support/Assert.h"
#include <fstream>
#include <google/protobuf/util/json_util.h>
#include <memory>
#include <optional>

#include "mlir/Dialect/Stencil/IIR/IIRAttributes.h"
#include "mlir/Dialect/Stencil/IIR/IIRDialect.h"
#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Diagnostics.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/Operation.h"
#include "mlir/IR/OperationSupport.h"
#include "mlir/Parser.h"

#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/SourceMgr.h"

namespace {

mlir::iir::Interval makeMLIRInterval(sir::Interval sirInterval) {
  return {sirInterval.LowerLevel, sirInterval.LowerOffset, sirInterval.UpperLevel,
          sirInterval.UpperOffset};
}

mlir::Identifier makeMLIRIdentifier(int dawnID, mlir::Builder& builder) {
  std::string idSuffix =
      dawnID < 0 ? (std::string("_") + std::to_string(-dawnID)) : std::to_string(dawnID);
  return mlir::Identifier::get("_" + idSuffix, builder.getContext());
}

mlir::iir::IntervalAttr makeMLIRIntervalAttr(sir::Interval sirInterval,
                                             mlir::MLIRContext* context) {
  return mlir::iir::IntervalAttr::get(makeMLIRInterval(sirInterval), context);
}

} // namespace

namespace dawn {
static void setCache(proto::iir::Cache* protoCache, const iir::Cache& cache) {
  protoCache->set_accessid(cache.getCachedFieldAccessID());
  switch(cache.getIOPolicy()) {
  case iir::Cache::IOPolicy::bpfill:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_BPFill);
    break;
  case iir::Cache::IOPolicy::epflush:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_EPFlush);
    break;
  case iir::Cache::IOPolicy::fill:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_Fill);
    break;
  case iir::Cache::IOPolicy::fill_and_flush:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_FillFlush);
    break;
  case iir::Cache::IOPolicy::flush:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_Flush);
    break;
  case iir::Cache::IOPolicy::local:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_Local);
    break;
  case iir::Cache::IOPolicy::unknown:
    protoCache->set_policy(proto::iir::Cache_CachePolicy_CP_Unknown);
    break;
  default:
    dawn_unreachable("unknown cache policy");
  }
  switch(cache.getType()) {
  case iir::Cache::CacheType::bypass:
    protoCache->set_type(proto::iir::Cache_CacheType_CT_Bypass);
    break;
  case iir::Cache::CacheType::IJ:
    protoCache->set_type(proto::iir::Cache_CacheType_CT_IJ);
    break;
  case iir::Cache::CacheType::IJK:
    protoCache->set_type(proto::iir::Cache_CacheType_CT_IJK);
    break;
  case iir::Cache::CacheType::K:
    protoCache->set_type(proto::iir::Cache_CacheType_CT_K);
    break;
  default:
    dawn_unreachable("unknown cache type");
  }
  if(cache.getInterval()) {
    auto sirInterval = cache.getInterval()->asSIRInterval();
    setInterval(protoCache->mutable_interval(), &sirInterval);
  }
  if(cache.getEnclosingAccessedInterval()) {
    auto sirInterval = cache.getEnclosingAccessedInterval()->asSIRInterval();
    setInterval(protoCache->mutable_enclosingaccessinterval(), &sirInterval);
  }
  if(cache.getWindow()) {
    protoCache->mutable_cachewindow()->set_minus(cache.getWindow()->m_m);
    protoCache->mutable_cachewindow()->set_plus(cache.getWindow()->m_p);
  }
}

static mlir::iir::CacheAttr makeMLIRCacheAttr(const iir::Cache& cache, mlir::OpBuilder& builder) {
  mlir::iir::Cache cacheAttr{};
  cacheAttr.accessID = cache.getCachedFieldAccessID();

  mlir::iir::CachePolicy cachePolicy;
  switch(cache.getIOPolicy()) {
  case iir::Cache::IOPolicy::bpfill:
    cachePolicy = mlir::iir::CachePolicy::BPFill;
    break;
  case iir::Cache::IOPolicy::epflush:
    cachePolicy = mlir::iir::CachePolicy::EPFlush;
    break;
  case iir::Cache::IOPolicy::fill:
    cachePolicy = mlir::iir::CachePolicy::Fill;
    break;
  case iir::Cache::IOPolicy::fill_and_flush:
    cachePolicy = mlir::iir::CachePolicy::FillFlush;
    break;
  case iir::Cache::IOPolicy::flush:
    cachePolicy = mlir::iir::CachePolicy::Flush;
    break;
  case iir::Cache::IOPolicy::local:
    cachePolicy = mlir::iir::CachePolicy::Local;
    break;
  case iir::Cache::IOPolicy::unknown:
    cachePolicy = mlir::iir::CachePolicy::Unknown;
    break;
  default:
    dawn_unreachable("unknown cache policy");
  }
  cacheAttr.policy = cachePolicy;

  mlir::iir::CacheType cacheType;
  switch(cache.getType()) {
  case iir::Cache::CacheType::bypass:
    cacheType = mlir::iir::CacheType::Bypass;
    break;
  case iir::Cache::CacheType::IJ:
    cacheType = mlir::iir::CacheType::IJ;
    break;
  case iir::Cache::CacheType::IJK:
    cacheType = mlir::iir::CacheType::IJK;
    break;
  case iir::Cache::CacheType::K:
    cacheType = mlir::iir::CacheType::K;
    break;
  default:
    dawn_unreachable("unknown cache type");
  }
  cacheAttr.type = cacheType;

  if(cache.getInterval()) {
    auto sirInterval = cache.getInterval()->asSIRInterval();
    cacheAttr.interval = makeMLIRInterval(sirInterval);
  }

  if(cache.getEnclosingAccessedInterval()) {
    auto sirInterval = cache.getEnclosingAccessedInterval()->asSIRInterval();
    cacheAttr.enclosingAccessInterval = makeMLIRInterval(sirInterval);
  }

  if(cache.getWindow()) {
    mlir::iir::Window window = {cache.getWindow()->m_m, cache.getWindow()->m_p};
    cacheAttr.window = window;
  }

  return mlir::iir::CacheAttr::get(cacheAttr, builder.getContext());
}

static iir::Cache makeCache(const proto::iir::Cache* protoCache) {
  iir::Cache::CacheType cacheType;
  iir::Cache::IOPolicy cachePolicy;
  std::optional<iir::Interval> interval;
  std::optional<iir::Interval> enclosingInverval;
  std::optional<iir::Cache::window> cacheWindow;
  int ID = protoCache->accessid();
  switch(protoCache->type()) {
  case proto::iir::Cache_CacheType_CT_Bypass:
    cacheType = iir::Cache::CacheType::bypass;
    break;
  case proto::iir::Cache_CacheType_CT_IJ:
    cacheType = iir::Cache::CacheType::IJ;
    break;
  case proto::iir::Cache_CacheType_CT_IJK:
    cacheType = iir::Cache::CacheType::IJK;
    break;
  case proto::iir::Cache_CacheType_CT_K:
    cacheType = iir::Cache::CacheType::K;
    break;
  default:
    dawn_unreachable("unknow cache type");
  }
  switch(protoCache->policy()) {
  case proto::iir::Cache_CachePolicy_CP_BPFill:
    cachePolicy = iir::Cache::IOPolicy::bpfill;
    break;
  case proto::iir::Cache_CachePolicy_CP_EPFlush:
    cachePolicy = iir::Cache::IOPolicy::epflush;
    break;
  case proto::iir::Cache_CachePolicy_CP_Fill:
    cachePolicy = iir::Cache::IOPolicy::fill;
    break;
  case proto::iir::Cache_CachePolicy_CP_FillFlush:
    cachePolicy = iir::Cache::IOPolicy::fill_and_flush;
    break;
  case proto::iir::Cache_CachePolicy_CP_Flush:
    cachePolicy = iir::Cache::IOPolicy::flush;
    break;
  case proto::iir::Cache_CachePolicy_CP_Local:
    cachePolicy = iir::Cache::IOPolicy::local;
    break;
  case proto::iir::Cache_CachePolicy_CP_Unknown:
    cachePolicy = iir::Cache::IOPolicy::unknown;
    break;
  default:
    dawn_unreachable("unknown cache policy");
  }
  if(protoCache->has_interval()) {
    interval = std::make_optional(*makeInterval(protoCache->interval()));
  }
  if(protoCache->has_enclosingaccessinterval()) {
    enclosingInverval = std::make_optional(*makeInterval(protoCache->enclosingaccessinterval()));
  }
  if(protoCache->has_cachewindow()) {
    cacheWindow = std::make_optional(
        iir::Cache::window{protoCache->cachewindow().plus(), protoCache->cachewindow().minus()});
  }

  return iir::Cache(cacheType, cachePolicy, ID, interval, enclosingInverval, cacheWindow);
}

static void computeInitialDerivedInfo(const std::shared_ptr<iir::StencilInstantiation>& target) {
  for(const auto& leaf : iterateIIROver<iir::DoMethod>(*target->getIIR())) {
    leaf->update(iir::NodeUpdateType::levelAndTreeAbove);
  }
}

void IIRSerializer::serializeMetaData(proto::iir::StencilInstantiation& target,
                                      iir::StencilMetaInformation& metaData) {
  auto protoMetaData = target.mutable_metadata();

  // Filling Field: map<int32, string> AccessIDToName = 1;
  auto& protoAccessIDtoNameMap = *protoMetaData->mutable_accessidtoname();
  for(const auto& accessIDtoNamePair : metaData.getAccessIDToNameMap()) {
    protoAccessIDtoNameMap.insert({accessIDtoNamePair.first, accessIDtoNamePair.second});
  }
  // Filling Field: repeated AccessIDType = 2;
  auto& protoAccessIDType = *protoMetaData->mutable_accessidtotype();
  for(const auto& accessIDTypePair : metaData.fieldAccessMetadata_.accessIDType_) {
    protoAccessIDType.insert({accessIDTypePair.first, (int)accessIDTypePair.second});
  }
  // Filling Field: map<int32, string> LiteralIDToName = 3;
  auto& protoLiteralIDToNameMap = *protoMetaData->mutable_literalidtoname();
  for(const auto& literalIDtoNamePair : metaData.fieldAccessMetadata_.LiteralAccessIDToNameMap_) {
    protoLiteralIDToNameMap.insert({literalIDtoNamePair.first, literalIDtoNamePair.second});
  }
  // Filling Field: repeated int32 FieldAccessIDs = 4;
  for(int fieldAccessID : metaData.fieldAccessMetadata_.FieldAccessIDSet_) {
    protoMetaData->add_fieldaccessids(fieldAccessID);
  }
  // Filling Field: repeated int32 APIFieldIDs = 5;
  for(int apifieldID : metaData.fieldAccessMetadata_.apiFieldIDs_) {
    protoMetaData->add_apifieldids(apifieldID);
  }
  // Filling Field: repeated int32 TemporaryFieldIDs = 6;
  for(int temporaryFieldID : metaData.fieldAccessMetadata_.TemporaryFieldAccessIDSet_) {
    protoMetaData->add_temporaryfieldids(temporaryFieldID);
  }
  // Filling Field: repeated int32 GlobalVariableIDs = 7;
  for(int globalVariableID : metaData.fieldAccessMetadata_.GlobalVariableAccessIDSet_) {
    protoMetaData->add_globalvariableids(globalVariableID);
  }

  // Filling Field: VariableVersions versionedFields = 8;
  auto protoVariableVersions = protoMetaData->mutable_versionedfields();
  auto& protoVariableVersionMap = *protoVariableVersions->mutable_variableversionmap();
  auto variableVersions = metaData.fieldAccessMetadata_.variableVersions_;
  for(const auto& IDtoVectorOfVersionsPair : variableVersions.getvariableVersionsMap()) {
    proto::iir::AllVersionedFields protoFieldVersions;
    for(int id : *(IDtoVectorOfVersionsPair.second)) {
      protoFieldVersions.add_allids(id);
    }
    protoVariableVersionMap.insert({IDtoVectorOfVersionsPair.first, protoFieldVersions});
  }

  // Filling Field:
  // map<string, dawn.proto.statements.BoundaryConditionDeclStmt> FieldnameToBoundaryCondition = 9;
  auto& protoFieldNameToBC = *protoMetaData->mutable_fieldnametoboundarycondition();
  for(auto fieldNameToBC : metaData.fieldnameToBoundaryConditionMap_) {
    proto::statements::Stmt protoStencilCall;
    ProtoStmtBuilder builder(&protoStencilCall, ast::StmtData::IIR_DATA_TYPE);
    fieldNameToBC.second->accept(builder);
    protoFieldNameToBC.insert({fieldNameToBC.first, protoStencilCall});
  }

  // Filling Field: map<int32, Array3i> fieldIDtoLegalDimensions = 10;
  auto& protoInitializedDimensionsMap = *protoMetaData->mutable_fieldidtolegaldimensions();
  for(auto IDToLegalDimension : metaData.fieldIDToInitializedDimensionsMap_) {
    proto::iir::Array3i array;
    array.set_int1(IDToLegalDimension.second[0]);
    array.set_int2(IDToLegalDimension.second[1]);
    array.set_int3(IDToLegalDimension.second[2]);
    protoInitializedDimensionsMap.insert({IDToLegalDimension.first, array});
  }

  // Filling Field: map<int32, dawn.proto.statements.StencilCallDeclStmt> IDToStencilCall = 11;
  auto& protoIDToStencilCallMap = *protoMetaData->mutable_idtostencilcall();
  for(auto IDToStencilCall : metaData.getStencilIDToStencilCallMap().getDirectMap()) {
    proto::statements::Stmt protoStencilCall;
    ProtoStmtBuilder builder(&protoStencilCall, ast::StmtData::IIR_DATA_TYPE);
    IDToStencilCall.second->accept(builder);
    protoIDToStencilCallMap.insert({IDToStencilCall.first, protoStencilCall});
  }

  // Filling Field: map<int32, Extents> boundaryCallToExtent = 12;
  auto& protoBoundaryCallToExtent = *protoMetaData->mutable_boundarycalltoextent();
  for(auto boundaryCallToExtent : metaData.boundaryConditionToExtentsMap_)
    protoBoundaryCallToExtent.insert(
        {boundaryCallToExtent.first->getID(), makeProtoExtents(boundaryCallToExtent.second)});

  // Filling Field: dawn.proto.statements.SourceLocation stencilLocation = 13;
  for(auto allocatedFieldID : metaData.fieldAccessMetadata_.AllocatedFieldAccessIDSet_) {
    protoMetaData->add_allocatedfieldids(allocatedFieldID);
  }

  // Filling Field: dawn.proto.statements.SourceLocation stencilLocation = 14;
  auto protoStencilLoc = protoMetaData->mutable_stencillocation();
  protoStencilLoc->set_column(metaData.stencilLocation_.Column);
  protoStencilLoc->set_line(metaData.stencilLocation_.Line);

  // Filling Field: string stencilMName = 15;
  protoMetaData->set_stencilname(metaData.stencilName_);
}

void IIRSerializer::serializeMetaDataToMLIR(mlir::iir::IIROp& iirOp,
                                            iir::StencilMetaInformation& metaData) {
  mlir::Builder builder(iirOp.getContext());
  {
    llvm::SmallVector<mlir::NamedAttribute, 3> accessIDToName;
    for(const auto& accessIDtoNamePair : metaData.getAccessIDToNameMap()) {
      mlir::Identifier accessID = makeMLIRIdentifier(accessIDtoNamePair.first, builder);
      mlir::Attribute name = builder.getStringAttr(accessIDtoNamePair.second);
      accessIDToName.push_back({accessID, name});
    }
    auto accessIDToNameAttr = builder.getDictionaryAttr(accessIDToName);
    iirOp.setAttr(mlir::iir::IIROp::getAccessIDToNameAttrName(), accessIDToNameAttr);
  }
  {
    llvm::SmallVector<mlir::NamedAttribute, 3> accessIDType;
    for(const auto& accessIDTypePair : metaData.fieldAccessMetadata_.accessIDType_) {
      mlir::Identifier accessID =
          builder.getIdentifier("_" + std::to_string(accessIDTypePair.first));
      mlir::Attribute type = builder.getI64IntegerAttr(static_cast<int>(accessIDTypePair.second));
      accessIDType.push_back({accessID, type});
    }
    auto accessIDTypeAttr = builder.getDictionaryAttr(accessIDType);
    iirOp.setAttr(mlir::iir::IIROp::getAccessIDTypeAttrName(), accessIDTypeAttr);
  }
  {
    llvm::SmallVector<mlir::NamedAttribute, 3> literalIDToName;
    for(const auto& literalIDtoNamePair : metaData.fieldAccessMetadata_.LiteralAccessIDToNameMap_) {
      mlir::Identifier literalID = makeMLIRIdentifier(literalIDtoNamePair.first, builder);
      mlir::Attribute name = builder.getStringAttr(literalIDtoNamePair.second);
      literalIDToName.push_back({literalID, name});
    }
    auto literalIDToNameAttr = builder.getDictionaryAttr(literalIDToName);
    iirOp.setAttr(mlir::iir::IIROp::getLiteralIDToNameAttrName(), literalIDToNameAttr);
  }
  {
    llvm::SmallVector<int64_t, 3> fieldAccessIDs;
    for(int fieldAccessID : metaData.fieldAccessMetadata_.FieldAccessIDSet_) {
      fieldAccessIDs.push_back(fieldAccessID);
    }
    iirOp.setAttr(mlir::iir::IIROp::getFieldAccessIDsAttrName(),
                  builder.getI64ArrayAttr(fieldAccessIDs));
  }
  {
    llvm::SmallVector<int64_t, 3> apiFieldIDs;
    for(int apifieldID : metaData.fieldAccessMetadata_.apiFieldIDs_) {
      apiFieldIDs.push_back(apifieldID);
    }
    iirOp.setAttr(mlir::iir::IIROp::getAPIFieldIDsAttrName(), builder.getI64ArrayAttr(apiFieldIDs));
  }
  {
    llvm::SmallVector<int64_t, 3> temporaryFieldIDs;
    for(int temporaryFieldID : metaData.fieldAccessMetadata_.TemporaryFieldAccessIDSet_) {
      temporaryFieldIDs.push_back(temporaryFieldID);
    }
    iirOp.setAttr(mlir::iir::IIROp::getTemporaryFieldIDsAttrName(),
                  builder.getI64ArrayAttr(temporaryFieldIDs));
  }
  {
    llvm::SmallVector<int64_t, 3> globalVariableIDs;
    for(int globalVariableID : metaData.fieldAccessMetadata_.GlobalVariableAccessIDSet_) {
      globalVariableIDs.push_back(globalVariableID);
    }
    iirOp.setAttr(mlir::iir::IIROp::getGlobalVariableIDsAttrName(),
                  builder.getI64ArrayAttr(globalVariableIDs));
  }
  {
    auto variableVersions = metaData.fieldAccessMetadata_.variableVersions_;
    llvm::SmallVector<mlir::NamedAttribute, 3> variableVersionsMap;
    for(const auto& IDtoVectorOfVersionsPair : variableVersions.getvariableVersionsMap()) {
      llvm::SmallVector<int64_t, 3> fieldVersions;
      for(int id : *(IDtoVectorOfVersionsPair.second)) {
        fieldVersions.push_back(id);
      }
      mlir::Identifier id =
          builder.getIdentifier("_" + std::to_string(IDtoVectorOfVersionsPair.first));
      mlir::Attribute fieldVersionsAttr = builder.getI64ArrayAttr(fieldVersions);
      variableVersionsMap.push_back({id, fieldVersionsAttr});
    }
    iirOp.setAttr(mlir::iir::IIROp::getVariableVersionsAttrName(),
                  builder.getDictionaryAttr(variableVersionsMap));
  }

  // TODO Handle boundary conditions
  /*
  // Filling Field:
  // map<string, dawn.proto.statements.BoundaryConditionDeclStmt> FieldnameToBoundaryCondition = 11;
  auto& protoFieldNameToBC = *protoMetaData->mutable_fieldnametoboundarycondition();
  for(auto fieldNameToBC : metaData.fieldnameToBoundaryConditionMap_) {
    proto::statements::Stmt protoStencilCall;
    ProtoStmtBuilder builder(&protoStencilCall);
    fieldNameToBC.second->accept(builder);
    protoFieldNameToBC.insert({fieldNameToBC.first, protoStencilCall});
  }
  */

  {
    llvm::SmallVector<mlir::NamedAttribute, 3> initializedDimensionsMap;
    for(auto IDToLegalDimension : metaData.fieldIDToInitializedDimensionsMap_) {
      llvm::SmallVector<int64_t, 3> array{
          IDToLegalDimension.second[0], IDToLegalDimension.second[1], IDToLegalDimension.second[2]};
      mlir::Identifier id = builder.getIdentifier("_" + std::to_string(IDToLegalDimension.first));
      mlir::Attribute initializedDimensions = builder.getI64ArrayAttr(array);
      initializedDimensionsMap.push_back({id, initializedDimensions});
    }
    iirOp.setAttr(mlir::iir::IIROp::getFieldIDToLegalDimensionsAttrName(),
                  builder.getDictionaryAttr(initializedDimensionsMap));
  }

  {
    mlir::OpBuilder idToStencilCallBuilder(iirOp.getIDToStencilCall().getTerminator());
    for(const auto& IDToStencilCall : metaData.getStencilIDToStencilCallMap().getDirectMap()) {
      auto idToStencilCallEntry = idToStencilCallBuilder.create<mlir::iir::IDToStencilCallEntryOp>(
          idToStencilCallBuilder.getUnknownLoc(), IDToStencilCall.first);
      mlir::OpBuilder idToStencilCallEntryBuilder(idToStencilCallEntry.getBody().getTerminator());
      MLIRStmtBuilder stmtBuilder(idToStencilCallEntryBuilder);
      IDToStencilCall.second->accept(stmtBuilder);
    }
  }

  // TODO Handle boundary call to extent
  /*
  // Filling Field: map<int32, Extents> boundaryCallToExtent = 14;
  auto& protoBoundaryCallToExtent = *protoMetaData->mutable_boundarycalltoextent();
  for(auto boundaryCallToExtent : metaData.boundaryConditionToExtentsMap_)
    protoBoundaryCallToExtent.insert(
        {boundaryCallToExtent.first->getID(), makeProtoExtents(boundaryCallToExtent.second)});
  */

  {
    llvm::SmallVector<int64_t, 3> allocatedFieldIDs;
    for(auto allocatedFieldID : metaData.fieldAccessMetadata_.AllocatedFieldAccessIDSet_) {
      allocatedFieldIDs.push_back(allocatedFieldID);
    }
    iirOp.setAttr(mlir::iir::IIROp::getAllocatedFieldIDsAttrName(),
                  builder.getI64ArrayAttr(allocatedFieldIDs));
  }

  // TODO Handle stencil location
  /*
  // Filling Field: dawn.proto.statements.SourceLocation stencilLocation = 16;
  auto protoStencilLoc = protoMetaData->mutable_stencillocation();
  protoStencilLoc->set_column(metaData.stencilLocation_.Column);
  protoStencilLoc->set_line(metaData.stencilLocation_.Line);
  */

  iirOp.setAttr(mlir::iir::IIROp::getStencilNameAttrName(),
                builder.getStringAttr(metaData.stencilName_));
}

void IIRSerializer::serializeIIR(proto::iir::StencilInstantiation& target,
                                 const std::unique_ptr<iir::IIR>& iir,
                                 std::set<std::string> const& usedBC) {
  auto protoIIR = target.mutable_internalir();

  auto& protoGlobalVariableMap = *protoIIR->mutable_globalvariabletovalue();
  for(auto& globalToValue : iir->getGlobalVariableMap()) {
    proto::iir::GlobalValueAndType protoGlobalToStore;
    bool valueIsSet = false;

    switch(globalToValue.second->getType()) {
    case sir::Value::Kind::Boolean:
      if(globalToValue.second->has_value()) {
        protoGlobalToStore.set_value(globalToValue.second->getValue<bool>());
        valueIsSet = true;
      }
      protoGlobalToStore.set_type(proto::iir::GlobalValueAndType_TypeKind_Boolean);
      break;
    case sir::Value::Kind::Integer:
      if(globalToValue.second->has_value()) {
        protoGlobalToStore.set_value(globalToValue.second->getValue<int>());
        valueIsSet = true;
      }
      protoGlobalToStore.set_type(proto::iir::GlobalValueAndType_TypeKind_Integer);
      break;
    case sir::Value::Kind::Double:
      if(globalToValue.second->has_value()) {
        protoGlobalToStore.set_value(globalToValue.second->getValue<double>());
        valueIsSet = true;
      }
      protoGlobalToStore.set_type(proto::iir::GlobalValueAndType_TypeKind_Double);
      break;
    default:
      dawn_unreachable("non-supported global type");
    }

    protoGlobalToStore.set_valueisset(valueIsSet);
    protoGlobalVariableMap.insert({globalToValue.first, protoGlobalToStore});
  }

  // Get all the stencils
  for(const auto& stencils : iir->getChildren()) {
    // creation of a new protobuf stencil
    auto protoStencil = protoIIR->add_stencils();
    // Information other than the children
    protoStencil->set_stencilid(stencils->getStencilID());
    auto protoAttribute = protoStencil->mutable_attr();
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeDoMethods)) {
      protoAttribute->add_attributes(
          proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeDoMethods);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeStages)) {
      protoAttribute->add_attributes(
          proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeStages);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeTemporaries)) {
      protoAttribute->add_attributes(
          proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeTemporaries);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::NoCodeGen)) {
      protoAttribute->add_attributes(
          proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_NoCodeGen);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::UseKCaches)) {
      protoAttribute->add_attributes(
          proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_UseKCaches);
    }

    // adding it's children
    for(const auto& multistages : stencils->getChildren()) {
      // creation of a protobuf multistage
      auto protoMSS = protoStencil->add_multistages();
      // Information other than the children
      if(multistages->getLoopOrder() == dawn::iir::LoopOrderKind::Forward) {
        protoMSS->set_looporder(proto::iir::MultiStage::Forward);
      } else if(multistages->getLoopOrder() == dawn::iir::LoopOrderKind::Backward) {
        protoMSS->set_looporder(proto::iir::MultiStage::Backward);
      } else {
        protoMSS->set_looporder(proto::iir::MultiStage::Parallel);
      }

      protoMSS->set_multistageid(multistages->getID());

      auto& protoMSSCacheMap = *protoMSS->mutable_caches();
      for(const auto& IDCachePair : multistages->getCaches()) {
        proto::iir::Cache protoCache;
        setCache(&protoCache, IDCachePair.second);
        protoMSSCacheMap.insert({IDCachePair.first, protoCache});
      }

      // adding it's children
      for(const auto& stages : multistages->getChildren()) {
        auto protoStage = protoMSS->add_stages();
        // Information other than the children
        protoStage->set_stageid(stages->getStageID());

        // adding it's children
        for(const auto& domethod : stages->getChildren()) {
          auto protoDoMethod = protoStage->add_domethods();
          // Information other than the children
          dawn::sir::Interval interval = domethod->getInterval().asSIRInterval();
          setInterval(protoDoMethod->mutable_interval(), &interval);
          protoDoMethod->set_domethodid(domethod->getID());

          auto protoStmt = protoDoMethod->mutable_ast();
          ProtoStmtBuilder builder(protoStmt, ast::StmtData::IIR_DATA_TYPE);
          auto ptr = std::make_shared<ast::BlockStmt>(
              domethod->getAST()); // TODO takes a copy to allow using shared_from_this()
          ptr->accept(builder);
        }
      }
    }
  }

  // Filling Field: repeated StencilDescStatement stencilDescStatements = 10;
  for(const auto& stencilDescStmt : iir->getControlFlowDescriptor().getStatements()) {
    auto protoStmt = protoIIR->add_controlflowstatements();
    ProtoStmtBuilder builder(protoStmt, ast::StmtData::IIR_DATA_TYPE);
    stencilDescStmt->accept(builder);
    if(stencilDescStmt->getData<iir::IIRStmtData>().StackTrace)
      DAWN_ASSERT_MSG(stencilDescStmt->getData<iir::IIRStmtData>().StackTrace->empty(),
                      "there should be no stack trace if inlining worked");
  }
  for(const auto& sf : iir->getStencilFunctions()) {
    if(usedBC.count(sf->Name) > 0) {
      auto protoBC = protoIIR->add_boundaryconditions();
      protoBC->set_name(sf->Name);
      for(auto& arg : sf->Args) {
        DAWN_ASSERT(arg->Kind == sir::StencilFunctionArg::ArgumentKind::Field);
        protoBC->add_args(arg->Name);
      }

      DAWN_ASSERT(sf->Asts.size() == 1);
      ProtoStmtBuilder builder(protoBC->mutable_aststmt(), ast::StmtData::IIR_DATA_TYPE);
      sf->Asts[0]->accept(builder);
    }
  }
}

void IIRSerializer::serializeToMLIR(mlir::iir::IIROp& iirOp, const std::unique_ptr<iir::IIR>& iir) {
  // Create global variables
  mlir::OpBuilder builder(iirOp.getContext());

  llvm::SmallVector<mlir::NamedAttribute, 3> globalVariableToStore;
  for(auto& globalToValue : iir->getGlobalVariableMap()) {
    llvm::SmallVector<mlir::NamedAttribute, 3> globalVariableToValue;

    bool valueIsSet = false;
    mlir::Identifier typeId = builder.getIdentifier("type");
    mlir::Identifier valueId = builder.getIdentifier("value");
    mlir::Identifier valueIsSetId = builder.getIdentifier("valueIsSet");

    switch(globalToValue.second->getType()) {
    case sir::Value::Kind::Boolean:
      globalVariableToValue.push_back({typeId, builder.getI64IntegerAttr(static_cast<int64_t>(
                                                   mlir::iir::GlobalType::Boolean))});
      if(globalToValue.second->has_value()) {
        globalVariableToValue.push_back(
            {valueId, builder.getBoolAttr(globalToValue.second->getValue<bool>())});
        valueIsSet = true;
      }
      break;
    case sir::Value::Kind::Integer:
      globalVariableToValue.push_back({typeId, builder.getI64IntegerAttr(static_cast<int64_t>(
                                                   mlir::iir::GlobalType::Integer))});
      if(globalToValue.second->has_value()) {
        globalVariableToValue.push_back(
            {valueId, builder.getI64IntegerAttr(globalToValue.second->getValue<int>())});
        valueIsSet = true;
      }
      break;
    case sir::Value::Kind::Double:
      globalVariableToValue.push_back(
          {typeId, builder.getI64IntegerAttr(static_cast<int64_t>(mlir::iir::GlobalType::Double))});
      if(globalToValue.second->has_value()) {
        globalVariableToValue.push_back(
            {valueId, builder.getF64FloatAttr(globalToValue.second->getValue<double>())});
        valueIsSet = true;
      }
      break;
    default:
      dawn_unreachable("non-supported global type");
    }

    globalVariableToValue.push_back({valueIsSetId, builder.getBoolAttr(valueIsSet)});
    globalVariableToStore.push_back({builder.getIdentifier(globalToValue.first),
                                     builder.getDictionaryAttr(globalVariableToValue)});
  }
  iirOp.setAttr(mlir::iir::IIROp::getGlobalVariableToValueAttrName(),
                builder.getDictionaryAttr(globalVariableToStore));

  // Get all the stencils
  for(const auto& stencils : iir->getChildren()) {
    // Populate MLIR stencil attributes
    llvm::SmallVector<mlir::iir::StencilAttribute, 3> stencilAttributes;

    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeDoMethods)) {
      stencilAttributes.push_back(mlir::iir::StencilAttribute::MergeDoMethods);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeStages)) {
      stencilAttributes.push_back(mlir::iir::StencilAttribute::MergeStages);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::MergeTemporaries)) {
      stencilAttributes.push_back(mlir::iir::StencilAttribute::MergeTemporaries);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::NoCodeGen)) {
      stencilAttributes.push_back(mlir::iir::StencilAttribute::NoCodeGen);
    }
    if(stencils->getStencilAttributes().has(sir::Attr::Kind::UseKCaches)) {
      stencilAttributes.push_back(mlir::iir::StencilAttribute::UseKCaches);
    }

    // Create stencils ops
    builder.setInsertionPoint(iirOp.getStencils().getTerminator());
    auto stencilOp = builder.create<mlir::iir::StencilOp>(
        builder.getUnknownLoc(), stencils->getStencilID(), stencilAttributes);

    // adding it's children
    for(const auto& multistages : stencils->getChildren()) {
      mlir::iir::LoopOrder loopOrderAttr;

      if(multistages->getLoopOrder() == dawn::iir::LoopOrderKind::Forward) {
        loopOrderAttr = mlir::iir::LoopOrder::Forward;
      } else if(multistages->getLoopOrder() == dawn::iir::LoopOrderKind::Backward) {
        loopOrderAttr = mlir::iir::LoopOrder::Backward;
      } else {
        loopOrderAttr = mlir::iir::LoopOrder::Parallel;
      }

      // Create multi-stage ops
      builder.setInsertionPoint(stencilOp.getBody().getTerminator());
      auto multiStageOp = builder.create<mlir::iir::MultiStageOp>(
          builder.getUnknownLoc(), multistages->getID(), loopOrderAttr);

      llvm::SmallVector<mlir::NamedAttribute, 3> caches;
      for(const auto& IDCachePair : multistages->getCaches()) {
        mlir::Identifier id = makeMLIRIdentifier(IDCachePair.first, builder);
        mlir::Attribute cacheAttr = makeMLIRCacheAttr(IDCachePair.second, builder);
        caches.push_back({id, cacheAttr});
      }
      multiStageOp.setAttr(mlir::iir::MultiStageOp::getCachesAttrName(),
                           builder.getDictionaryAttr(caches));

      // adding it's children
      for(const auto& stages : multistages->getChildren()) {
        // Create stage ops
        builder.setInsertionPoint(multiStageOp.getBody().getTerminator());
        auto stageOp =
            builder.create<mlir::iir::StageOp>(builder.getUnknownLoc(), stages->getStageID());

        // adding it's children
        for(const auto& domethod : stages->getChildren()) {
          dawn::sir::Interval interval = domethod->getInterval().asSIRInterval();

          // Create do methods ops
          builder.setInsertionPoint(stageOp.getBody().getTerminator());
          mlir::iir::IntervalAttr intervalAttr =
              makeMLIRIntervalAttr(interval, builder.getContext());
          auto doMethod = builder.create<mlir::iir::DoMethodOp>(builder.getUnknownLoc(),
                                                                domethod->getID(), intervalAttr);
          builder.setInsertionPoint(doMethod.getBody().getTerminator());

          MLIRStmtBuilder stmtBuilder(builder);
          auto ptr = std::make_shared<ast::BlockStmt>(domethod->getAST());
          ptr->accept(stmtBuilder);

          /*
          for(const auto& stmtaccesspair : domethod->getChildren()) {
            // Compute read and write extents
            llvm::SmallVector<int64_t, 7> readAccesses;
            llvm::SmallVector<int64_t, 7> writeAccesses;
            if(stmtaccesspair->getCallerAccesses()) {
              auto accesses = stmtaccesspair->getCallerAccesses();

              for(const auto& idExtentPair : accesses->getReadAccesses()) {
                readAccesses.push_back(idExtentPair.first);
                for(const auto& extentElem : idExtentPair.second.getExtents()) {
                  readAccesses.push_back(extentElem.Minus);
                  readAccesses.push_back(extentElem.Plus);
                }
              }

              for(const auto& idExtentPair : accesses->getWriteAccesses()) {
                writeAccesses.push_back(idExtentPair.first);
                for(const auto& extentElem : idExtentPair.second.getExtents()) {
                  writeAccesses.push_back(extentElem.Minus);
                  writeAccesses.push_back(extentElem.Plus);
                }
              }
            }

            // Create statement access pair op
            mlir::OpBuilder statementAccessPairBuilder(doMethodOp.getBody().getTerminator());
            auto statementAccessPairOp =
                statementAccessPairBuilder.create<mlir::iir::StatementAccessPairOp>(
                    statementAccessPairBuilder.getUnknownLoc(), readAccesses, writeAccesses);

            serializeStmtAccessPairToMLIR(statementAccessPairOp, stmtaccesspair);
          }*/
        }
      }
    }
  }

  for(const auto& stencilDeclStmt : iir->getControlFlowDescriptor().getStatements()) {
    mlir::OpBuilder controlFlowBuilder(iirOp.getControlFlow().getTerminator());
    MLIRStmtBuilder stmtBuilder(controlFlowBuilder);
    stencilDeclStmt->accept(stmtBuilder);
    if(stencilDeclStmt->getData<iir::IIRStmtData>().StackTrace)
      DAWN_ASSERT_MSG(stencilDeclStmt->getData<iir::IIRStmtData>().StackTrace->empty(),
                      "there should be no stack trace if inlining worked");
  }

  // TODO: Handle boundary conditions
  /*
  for(const auto& sf : iir->getStencilFunctions()) {
    if(usedBC.count(sf->Name) > 0) {
      auto protoBC = protoIIR->add_boundaryconditions();
      protoBC->set_name(sf->Name);
      for(auto& arg : sf->Args) {
        DAWN_ASSERT(arg->Kind == sir::StencilFunctionArg::AK_Field);
        protoBC->add_args(arg->Name);
      }

      DAWN_ASSERT(sf->Asts.size() == 1);
      ProtoStmtBuilder builder(protoBC->mutable_aststmt());
      sf->Asts[0]->accept(builder);
    }
  }*/
}

std::string
IIRSerializer::serializeImpl(const std::shared_ptr<iir::StencilInstantiation>& instantiation,
                             Format kind) {
  if(kind == dawn::IIRSerializer::Format::MLIR) {
    std::string outString;
    llvm::raw_string_ostream os(outString);

    // Register the IIR dialect and all the associated symbols
    static bool registered = false;
    if(!registered) {
      mlir::registerDialect<mlir::iir::IIRDialect>();
      registered = true;
    }

    mlir::MLIRContext context;
    mlir::Builder mlirBuilder(&context);

    // Create the top-level IIR operation
    mlir::OperationState state(mlirBuilder.getUnknownLoc(), mlir::iir::IIROp::getOperationName());
    mlir::iir::IIROp::build(&mlirBuilder, state);
    mlir::iir::IIROp iirOp = llvm::cast<mlir::iir::IIROp>(mlir::Operation::create(state));

    serializeMetaDataToMLIR(iirOp, instantiation->getMetaData());
    serializeToMLIR(iirOp, instantiation->getIIR());
    iirOp.setAttr(mlir::iir::IIROp::getFilenameAttrName(),
                  mlirBuilder.getStringAttr(instantiation->getMetaData().fileName_));

    iirOp.getOperation()->print(os);
    os << "\n";
    return os.str();
  }

  GOOGLE_PROTOBUF_VERIFY_VERSION;
  /////////////////////////////// WITTODO //////////////////////////////////////////////////////////
  //==------------------------------------------------------------------------------------------==//
  // After we have the merge of carlos' new inliner that distinguishes between full inlining (as
  // used for optimized code generation) and precomputation (store stencil function computation one
  // for one into temporaries), we need to make sure we use the latter as those expressions can be
  // properly flagged to be revertible and we can actually go back. An example here:
  //
  // stencil_function harm_avg{
  //     return 0.5*(u[i+1] + u[i-1]);
  // }
  // stencil_function upwind_flux{
  //     return u[j+1] - u[j]
  //}
  //
  // out = upwind_flux(harm_avg(u))
  //
  // can be represented either by [precomputation]:
  //
  // tmp_1 = 0.5*(u[i+1] + u[i-1])
  // tmp_2 = tmp_1[j+1] - tmp_1[j]
  // out = tmp_2
  //
  // or by [full inlining]
  //
  // out = 0.5*(u[i+1, j+1] + u[i-1, j+1]) - 0.5*(u[i+1] + u[i-1])
  //==------------------------------------------------------------------------------------------==//

  using namespace dawn::proto::iir;
  proto::iir::StencilInstantiation protoStencilInstantiation;
  serializeMetaData(protoStencilInstantiation, instantiation->getMetaData());
  auto& fieldNameToBCMap = instantiation->getMetaData().getFieldNameToBCMap();
  std::set<std::string> usedBC;
  std::transform(
      fieldNameToBCMap.begin(), fieldNameToBCMap.end(), std::inserter(usedBC, usedBC.end()),
      [](std::pair<std::string, std::shared_ptr<iir::BoundaryConditionDeclStmt>> const& bc) {
        return bc.second->getFunctor();
      });
  serializeIIR(protoStencilInstantiation, instantiation->getIIR(), usedBC);
  protoStencilInstantiation.set_filename(instantiation->getMetaData().fileName_);

  // Encode the message
  std::string str;
  switch(kind) {
  case Format::Json: {
    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    options.always_print_primitive_fields = true;
    options.preserve_proto_field_names = true;
    auto status =
        google::protobuf::util::MessageToJsonString(protoStencilInstantiation, &str, options);
    if(!status.ok())
      throw std::runtime_error(dawn::format("cannot serialize IIR: %s", status.ToString()));
    break;
  }
  case Format::Byte: {
    if(!protoStencilInstantiation.SerializeToString(&str))
      throw std::runtime_error(dawn::format("cannot serialize IIR:"));
    break;
  }
  // case dawn::IIRSerialize::SK_MLIR is handled above
  default:
    dawn_unreachable("invalid SerializationKind");
  }

  return str;
}

void IIRSerializer::deserializeMetaData(std::shared_ptr<iir::StencilInstantiation>& target,
                                        const proto::iir::StencilMetaInfo& protoMetaData) {
  auto& metadata = target->getMetaData();
  for(auto IDtoName : protoMetaData.accessidtoname()) {
    metadata.addAccessIDNamePair(IDtoName.first, IDtoName.second);
  }

  for(auto accessIDTypePair : protoMetaData.accessidtotype()) {
    metadata.fieldAccessMetadata_.accessIDType_.emplace(
        accessIDTypePair.first, (iir::FieldAccessType)accessIDTypePair.second);
  }

  for(auto literalIDToName : protoMetaData.literalidtoname()) {
    metadata.fieldAccessMetadata_.LiteralAccessIDToNameMap_[literalIDToName.first] =
        literalIDToName.second;
  }
  for(auto fieldaccessID : protoMetaData.fieldaccessids()) {
    metadata.fieldAccessMetadata_.FieldAccessIDSet_.insert(fieldaccessID);
  }
  for(auto ApiFieldID : protoMetaData.apifieldids()) {
    metadata.fieldAccessMetadata_.apiFieldIDs_.push_back(ApiFieldID);
  }
  for(auto temporaryFieldID : protoMetaData.temporaryfieldids()) {
    metadata.fieldAccessMetadata_.TemporaryFieldAccessIDSet_.insert(temporaryFieldID);
  }
  for(auto globalVariableID : protoMetaData.globalvariableids()) {
    metadata.fieldAccessMetadata_.GlobalVariableAccessIDSet_.insert(globalVariableID);
  }
  for(auto allocatedFieldID : protoMetaData.allocatedfieldids()) {
    metadata.fieldAccessMetadata_.AllocatedFieldAccessIDSet_.insert(allocatedFieldID);
  }

  for(auto variableVersionMap : protoMetaData.versionedfields().variableversionmap()) {
    for(auto versionedID : variableVersionMap.second.allids()) {
      metadata.addFieldVersionIDPair(variableVersionMap.first, versionedID);
    }
  }

  struct DeclStmtFinder : public iir::ASTVisitorForwarding {
    void visit(const std::shared_ptr<iir::StencilCallDeclStmt>& stmt) override {
      stencilCallDecl.insert(std::make_pair(stmt->getID(), stmt));
      ASTVisitorForwarding::visit(stmt);
    }
    void visit(const std::shared_ptr<iir::BoundaryConditionDeclStmt>& stmt) override {
      boundaryConditionDecl.insert(std::make_pair(stmt->getID(), stmt));
      ASTVisitorForwarding::visit(stmt);
    }
    std::map<int, std::shared_ptr<iir::StencilCallDeclStmt>> stencilCallDecl;
    std::map<int, std::shared_ptr<iir::BoundaryConditionDeclStmt>> boundaryConditionDecl;
  };
  DeclStmtFinder declStmtFinder;
  for(auto& stmt : target->getIIR()->getControlFlowDescriptor().getStatements())
    stmt->accept(declStmtFinder);

  for(auto IDToCall : protoMetaData.idtostencilcall()) {
    auto call = IDToCall.second;
    std::shared_ptr<ast::StencilCall> astStencilCall = std::make_shared<ast::StencilCall>(
        call.stencil_call_decl_stmt().stencil_call().callee(),
        makeLocation(call.stencil_call_decl_stmt().stencil_call()));
    for(const auto& protoFieldName : call.stencil_call_decl_stmt().stencil_call().arguments()) {
      astStencilCall->Args.push_back(protoFieldName);
    }

    auto stmt = declStmtFinder.stencilCallDecl[call.stencil_call_decl_stmt().id()];
    stmt->setID(call.stencil_call_decl_stmt().id());
    metadata.addStencilCallStmt(stmt, IDToCall.first);
  }

  for(auto FieldnameToBC : protoMetaData.fieldnametoboundarycondition()) {
    auto foundDecl = declStmtFinder.boundaryConditionDecl.find(
        FieldnameToBC.second.boundary_condition_decl_stmt().id());

    metadata.fieldnameToBoundaryConditionMap_[FieldnameToBC.first] =
        foundDecl != declStmtFinder.boundaryConditionDecl.end()
            ? foundDecl->second
            : dyn_pointer_cast<iir::BoundaryConditionDeclStmt>(
                  makeStmt(FieldnameToBC.second, ast::StmtData::IIR_DATA_TYPE));
  }

  for(auto fieldIDInitializedDims : protoMetaData.fieldidtolegaldimensions()) {
    Array3i dims{fieldIDInitializedDims.second.int1(), fieldIDInitializedDims.second.int2(),
                 fieldIDInitializedDims.second.int3()};
    metadata.fieldIDToInitializedDimensionsMap_[fieldIDInitializedDims.first] = dims;
  }

  for(auto boundaryCallToExtent : protoMetaData.boundarycalltoextent())
    metadata.boundaryConditionToExtentsMap_.insert(
        std::make_pair(declStmtFinder.boundaryConditionDecl.at(boundaryCallToExtent.first),
                       makeExtents(&boundaryCallToExtent.second)));

  metadata.stencilLocation_.Column = protoMetaData.stencillocation().column();
  metadata.stencilLocation_.Line = protoMetaData.stencillocation().line();

  metadata.stencilName_ = protoMetaData.stencilname();
}

void IIRSerializer::deserializeIIR(std::shared_ptr<iir::StencilInstantiation>& target,
                                   const proto::iir::IIR& protoIIR) {
  for(auto GlobalToValue : protoIIR.globalvariabletovalue()) {
    std::shared_ptr<sir::Value> value;
    switch(GlobalToValue.second.type()) {
    case proto::iir::GlobalValueAndType_TypeKind_Boolean:
      if(GlobalToValue.second.valueisset()) {
        value = std::make_shared<sir::Value>(sir::Value::Kind::Boolean);
      } else {
        value = std::make_shared<sir::Value>(GlobalToValue.second.value());
      }
      break;
    case proto::iir::GlobalValueAndType_TypeKind_Integer:
      if(GlobalToValue.second.valueisset()) {
        value = std::make_shared<sir::Value>(sir::Value::Kind::Integer);
      } else {
        // the explicit cast is needed since in this case GlobalToValue.second.value()
        // may hold a double constant because of trailing dot in the IIR (e.g. 12.)
        value = std::make_shared<sir::Value>((int)GlobalToValue.second.value());
      }
      break;
    case proto::iir::GlobalValueAndType_TypeKind_Double:
      if(GlobalToValue.second.valueisset()) {
        value = std::make_shared<sir::Value>(sir::Value::Kind::Double);
      } else {
        value = std::make_shared<sir::Value>((double)GlobalToValue.second.value());
      }
      break;
    default:
      dawn_unreachable("unsupported type");
    }

    target->getIIR()->insertGlobalVariable(GlobalToValue.first, value);
  }

  int stencilPos = 0;
  for(const auto& protoStencils : protoIIR.stencils()) {
    int mssPos = 0;
    sir::Attr attributes;

    target->getIIR()->insertChild(std::make_unique<iir::Stencil>(target->getMetaData(), attributes,
                                                                 protoStencils.stencilid()),
                                  target->getIIR());
    const auto& IIRStencil = target->getIIR()->getChild(stencilPos++);

    for(auto attribute : protoStencils.attr().attributes()) {
      if(attribute ==
         proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeDoMethods) {
        IIRStencil->getStencilAttributes().set(sir::Attr::Kind::MergeDoMethods);
      }
      if(attribute ==
         proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeStages) {
        IIRStencil->getStencilAttributes().set(sir::Attr::Kind::MergeStages);
      }
      if(attribute ==
         proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_MergeTemporaries) {
        IIRStencil->getStencilAttributes().set(sir::Attr::Kind::MergeTemporaries);
      }
      if(attribute ==
         proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_NoCodeGen) {
        IIRStencil->getStencilAttributes().set(sir::Attr::Kind::NoCodeGen);
      }
      if(attribute ==
         proto::iir::Attributes::StencilAttributes::Attributes_StencilAttributes_UseKCaches) {
        IIRStencil->getStencilAttributes().set(sir::Attr::Kind::UseKCaches);
      }
    }

    for(const auto& protoMSS : protoStencils.multistages()) {
      int stagePos = 0;
      iir::LoopOrderKind looporder;
      if(protoMSS.looporder() == proto::iir::MultiStage_LoopOrder::MultiStage_LoopOrder_Backward) {
        looporder = iir::LoopOrderKind::Backward;
      }
      if(protoMSS.looporder() == proto::iir::MultiStage_LoopOrder::MultiStage_LoopOrder_Forward) {
        looporder = iir::LoopOrderKind::Forward;
      }
      if(protoMSS.looporder() == proto::iir::MultiStage_LoopOrder::MultiStage_LoopOrder_Parallel) {
        looporder = iir::LoopOrderKind::Parallel;
      }
      (IIRStencil)
          ->insertChild(std::make_unique<iir::MultiStage>(target->getMetaData(), looporder));

      const auto& IIRMSS = (IIRStencil)->getChild(mssPos++);
      IIRMSS->setID(protoMSS.multistageid());

      for(const auto& IDCachePair : protoMSS.caches()) {
        IIRMSS->getCaches().insert({IDCachePair.first, makeCache(&IDCachePair.second)});
      }

      for(const auto& protoStage : protoMSS.stages()) {
        int doMethodPos = 0;
        int stageID = protoStage.stageid();

        IIRMSS->insertChild(std::make_unique<iir::Stage>(target->getMetaData(), stageID));
        const auto& IIRStage = IIRMSS->getChild(stagePos++);

        for(const auto& protoDoMethod : protoStage.domethods()) {
          IIRStage->insertChild(std::make_unique<iir::DoMethod>(
              *makeInterval(protoDoMethod.interval()), target->getMetaData()));

          auto& IIRDoMethod = (IIRStage)->getChild(doMethodPos++);
          IIRDoMethod->setID(protoDoMethod.domethodid());

          auto ast = std::dynamic_pointer_cast<iir::BlockStmt>(
              makeStmt(protoDoMethod.ast(), ast::StmtData::IIR_DATA_TYPE));
          DAWN_ASSERT(ast);
          IIRDoMethod->setAST(std::move(*ast));
        }
      }
    }
  }
  for(auto& controlFlowStmt : protoIIR.controlflowstatements()) {
    target->getIIR()->getControlFlowDescriptor().insertStmt(
        makeStmt(controlFlowStmt, ast::StmtData::IIR_DATA_TYPE));
  }
  for(auto& boundaryCondition : protoIIR.boundaryconditions()) {
    auto stencilFunction = std::make_shared<sir::StencilFunction>();
    stencilFunction->Name = boundaryCondition.name();

    for(auto& proto_arg : boundaryCondition.args()) {
      auto new_arg = std::make_shared<sir::StencilFunctionArg>();
      new_arg->Name = proto_arg;
      new_arg->Kind = sir::StencilFunctionArg::ArgumentKind::Field;
      stencilFunction->Args.push_back(std::move(new_arg));
    }
    auto stmt = std::dynamic_pointer_cast<iir::BlockStmt>(
        makeStmt(boundaryCondition.aststmt(), ast::StmtData::IIR_DATA_TYPE));
    DAWN_ASSERT(stmt);
    stencilFunction->Asts.push_back(std::make_shared<iir::AST>(stmt));

    target->getIIR()->insertStencilFunction(stencilFunction);
  }
}

void IIRSerializer::deserializeImpl(const std::string& str, IIRSerializer::Format kind,
                                    std::shared_ptr<iir::StencilInstantiation>& target) {
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  // Decode the string
  proto::iir::StencilInstantiation protoStencilInstantiation;
  switch(kind) {
  case dawn::IIRSerializer::Format::Json: {
    auto status = google::protobuf::util::JsonStringToMessage(str, &protoStencilInstantiation);
    if(!status.ok())
      throw std::runtime_error(
          dawn::format("cannot deserialize StencilInstantiation: %s", status.ToString()));
    break;
  }
  case dawn::IIRSerializer::Format::Byte: {
    if(!protoStencilInstantiation.ParseFromString(str))
      throw std::runtime_error(dawn::format("cannot deserialize StencilInstantiation: %s"));
    break;
  }
  // case dawn::IIRSerializer::SK_MLIR is handled above
  default:
    dawn_unreachable("invalid SerializationKind");
  }

  deserializeIIR(target, (protoStencilInstantiation.internalir()));
  deserializeMetaData(target, (protoStencilInstantiation.metadata()));
  target->getMetaData().fileName_ = protoStencilInstantiation.filename();
  computeInitialDerivedInfo(target);
}

std::shared_ptr<iir::StencilInstantiation> IIRSerializer::deserialize(const std::string& file,
                                                                      OptimizerContext* context,
                                                                      IIRSerializer::Format kind) {
  std::ifstream ifs(file);
  if(!ifs.is_open())
    throw std::runtime_error(
        dawn::format("cannot deserialize IIR: failed to open file \"%s\"", file));

  std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  std::shared_ptr<iir::StencilInstantiation> returnvalue =
      std::make_shared<iir::StencilInstantiation>();
  deserializeImpl(str, kind, returnvalue);
  return returnvalue;
}

std::shared_ptr<iir::StencilInstantiation>
IIRSerializer::deserializeFromString(const std::string& str, OptimizerContext* context,
                                     IIRSerializer::Format kind) {
  std::shared_ptr<iir::StencilInstantiation> returnvalue =
      std::make_shared<iir::StencilInstantiation>();
  deserializeImpl(str, kind, returnvalue);
  return returnvalue;
}

void IIRSerializer::serialize(const std::string& file,
                              const std::shared_ptr<iir::StencilInstantiation> instantiation,
                              dawn::IIRSerializer::Format kind) {
  std::ofstream ofs(file);
  if(!ofs.is_open())
    throw std::runtime_error(format("cannot serialize IIR: failed to open file \"%s\"", file));

  auto str = serializeImpl(instantiation, kind);
  std::copy(str.begin(), str.end(), std::ostreambuf_iterator<char>(ofs));
}

std::string
IIRSerializer::serializeToString(const std::shared_ptr<iir::StencilInstantiation> instantiation,
                                 dawn::IIRSerializer::Format kind) {
  return serializeImpl(instantiation, kind);
}

} // namespace dawn
