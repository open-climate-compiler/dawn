//===--------------------------------------------------------------------------------*- C++ -*-===//
//                          _
//                         | |
//                       __| | __ ___      ___ ___
//                      / _` |/ _` \ \ /\ / / '_  |
//                     | (_| | (_| |\ V  V /| | | |
//                      \__,_|\__,_| \_/\_/ |_| |_| - Compiler Toolchain
//
//
//  This file is distributed under the MIT License (MIT).
//  See LICENSE.txt for details.
//
//===------------------------------------------------------------------------------------------===//

#include "dawn/Serialization/ASTSerializer.h"
#include "SIR/statements.pb.h"
#include "dawn/AST/ASTStmt.h"
#include "dawn/IIR/ASTExpr.h"
#include "dawn/IIR/ASTStmt.h"
#include "dawn/SIR/ASTStmt.h"
#include <fstream>
#include <google/protobuf/util/json_util.h>
#include <iterator>
#include <list>
#include <memory>
#include <optional>
#include <tuple>
#include <utility>

#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/MLIRContext.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/Operation.h"
#include "mlir/IR/OperationSupport.h"
#include "llvm/Support/raw_ostream.h"

using namespace dawn;
using namespace ast;

namespace {
// TODO: This is the same as in IIRSerializer.cpp
mlir::Identifier makeMLIRIdentifier(int dawnID, mlir::Builder& builder) {
  std::string idSuffix =
      dawnID < 0 ? (std::string("_") + std::to_string(-dawnID)) : std::to_string(dawnID);
  return mlir::Identifier::get("_" + idSuffix, builder.getContext());
}

void fillData(iir::IIRStmtData& data, dawn::proto::statements::StmtData const& dataProto) {
  if(dataProto.has_accesses()) {
    iir::Accesses callerAccesses;
    for(auto writeAccess : dataProto.accesses().writeaccess()) {
      callerAccesses.addWriteExtent(writeAccess.first, makeExtents(&writeAccess.second));
    }
    for(auto readAccess : dataProto.accesses().readaccess()) {
      callerAccesses.addReadExtent(readAccess.first, makeExtents(&readAccess.second));
    }
    data.CallerAccesses = std::move(callerAccesses);
  }
}

std::unique_ptr<ast::StmtData> makeData(ast::StmtData::DataType dataType,
                                        dawn::proto::statements::StmtData const& dataProto) {
  if(dataType == ast::StmtData::SIR_DATA_TYPE)
    return std::make_unique<sir::SIRStmtData>();
  else {
    auto data = std::make_unique<iir::IIRStmtData>();
    fillData(*data, dataProto);
    return data;
  }
}

std::unique_ptr<ast::StmtData> makeData(ast::StmtData::DataType dataType) {
  if(dataType == ast::StmtData::SIR_DATA_TYPE)
    return std::make_unique<sir::SIRStmtData>();
  else {
    auto data = std::make_unique<iir::IIRStmtData>();
    return data;
  }
}

std::unique_ptr<ast::StmtData>
makeVarDeclStmtData(ast::StmtData::DataType dataType,
                    dawn::proto::statements::StmtData const& dataProto,
                    const dawn::proto::statements::VarDeclStmtData& varDeclStmtDataProto) {
  if(dataType == ast::StmtData::SIR_DATA_TYPE) {
    return std::make_unique<sir::SIRStmtData>();
  } else {
    auto data = std::make_unique<iir::VarDeclStmtData>();
    fillData(*data, dataProto);
    if(varDeclStmtDataProto.has_accessid())
      data->AccessID = std::make_optional(varDeclStmtDataProto.accessid().value());
    return data;
  }
}

void fillAccessExprDataFromProto(iir::IIRAccessExprData& data,
                                 const dawn::proto::statements::AccessExprData& dataProto) {
  if(dataProto.has_accessid())
    data.AccessID = std::make_optional(dataProto.accessid().value());
}

void setAccessExprData(dawn::proto::statements::AccessExprData* dataProto,
                       const iir::IIRAccessExprData& data) {
  if(data.AccessID) {
    auto accessID = dataProto->mutable_accessid();
    accessID->set_value(*data.AccessID);
  }
}

void setStmtData(proto::statements::StmtData* protoStmtData, iir::Stmt& stmt) {
  if(stmt.getDataType() == ast::StmtData::IIR_DATA_TYPE) {
    if(stmt.getData<iir::IIRStmtData>().CallerAccesses.has_value()) {
      setAccesses(protoStmtData->mutable_accesses(),
                  stmt.getData<iir::IIRStmtData>().CallerAccesses);
    }
    DAWN_ASSERT_MSG(!stmt.getData<iir::IIRStmtData>().CalleeAccesses,
                    "inlining did not work as we have callee-accesses");
  }
}

void setVarDeclStmtData(dawn::proto::statements::VarDeclStmtData* dataProto,
                        const iir::VarDeclStmt& stmt) {
  if(stmt.getDataType() == ast::StmtData::IIR_DATA_TYPE) {
    if(stmt.getData<iir::VarDeclStmtData>().AccessID) {
      auto accessID = dataProto->mutable_accessid();
      accessID->set_value(*stmt.getData<iir::VarDeclStmtData>().AccessID);
    }
  }
}
} // namespace

dawn::proto::statements::Extents makeProtoExtents(dawn::iir::Extents const& extents) {
  dawn::proto::statements::Extents protoExtents;
  extent_dispatch(
      extents.horizontalExtent(),
      [&](iir::CartesianExtent const& hExtent) {
        auto cartesianExtent = protoExtents.mutable_cartesian_extent();
        auto protoIExtent = cartesianExtent->mutable_i_extent();
        protoIExtent->set_minus(hExtent.iMinus());
        protoIExtent->set_plus(hExtent.iPlus());
        auto protoJExtent = cartesianExtent->mutable_j_extent();
        protoJExtent->set_minus(hExtent.jMinus());
        protoJExtent->set_plus(hExtent.jPlus());
      },
      [&](iir::UnstructuredExtent const& hExtent) {
        auto protoHExtent = protoExtents.mutable_unstructured_extent();
        protoHExtent->set_has_extent(hExtent.hasExtent());
      },
      [&] { protoExtents.mutable_zero_extent(); });

  auto const& vExtent = extents.verticalExtent();
  auto protoVExtent = protoExtents.mutable_vertical_extent();
  protoVExtent->set_minus(vExtent.minus());
  protoVExtent->set_plus(vExtent.plus());

  return protoExtents;
}

void setAccesses(dawn::proto::statements::Accesses* protoAccesses,
                 const std::optional<iir::Accesses>& accesses) {
  auto protoReadAccesses = protoAccesses->mutable_readaccess();
  for(auto IDExtentsPair : accesses->getReadAccesses())
    protoReadAccesses->insert({IDExtentsPair.first, makeProtoExtents(IDExtentsPair.second)});

  auto protoWriteAccesses = protoAccesses->mutable_writeaccess();
  for(auto IDExtentsPair : accesses->getWriteAccesses())
    protoWriteAccesses->insert({IDExtentsPair.first, makeProtoExtents(IDExtentsPair.second)});
}

iir::Extents makeExtents(const dawn::proto::statements::Extents* protoExtents) {
  using ProtoExtents = dawn::proto::statements::Extents;
  iir::Extent vExtent{protoExtents->vertical_extent().minus(),
                      protoExtents->vertical_extent().plus()};

  switch(protoExtents->horizontal_extent_case()) {
  case ProtoExtents::kCartesianExtent: {
    auto const& hExtent = protoExtents->cartesian_extent();
    return {iir::HorizontalExtent{ast::cartesian, hExtent.i_extent().minus(),
                                  hExtent.i_extent().plus(), hExtent.j_extent().minus(),
                                  hExtent.j_extent().plus()},
            vExtent};
  }
  case ProtoExtents::kUnstructuredExtent: {
    auto const& hExtent = protoExtents->unstructured_extent();
    return {iir::HorizontalExtent{ast::unstructured, hExtent.has_extent()}, vExtent};
  }
  case ProtoExtents::kZeroExtent:
    return iir::Extents{iir::HorizontalExtent{}, vExtent};
  default:
    dawn_unreachable("unknown extent");
  }
}

void setAST(dawn::proto::statements::AST* astProto, const AST* ast);

void setLocation(dawn::proto::statements::SourceLocation* locProto, const SourceLocation& loc) {
  locProto->set_column(loc.Column);
  locProto->set_line(loc.Line);
}

void setBuiltinType(dawn::proto::statements::BuiltinType* builtinTypeProto,
                    const BuiltinTypeID& builtinType) {
  builtinTypeProto->set_type_id(
      static_cast<dawn::proto::statements::BuiltinType_TypeID>(builtinType));
}

void setInterval(dawn::proto::statements::Interval* intervalProto, const sir::Interval* interval) {
  if(interval->LowerLevel == sir::Interval::Start)
    intervalProto->set_special_lower_level(dawn::proto::statements::Interval::Start);
  else if(interval->LowerLevel == sir::Interval::End)
    intervalProto->set_special_lower_level(dawn::proto::statements::Interval::End);
  else
    intervalProto->set_lower_level(interval->LowerLevel);

  if(interval->UpperLevel == sir::Interval::Start)
    intervalProto->set_special_upper_level(dawn::proto::statements::Interval::Start);
  else if(interval->UpperLevel == sir::Interval::End)
    intervalProto->set_special_upper_level(dawn::proto::statements::Interval::End);
  else
    intervalProto->set_upper_level(interval->UpperLevel);

  intervalProto->set_lower_offset(interval->LowerOffset);
  intervalProto->set_upper_offset(interval->UpperOffset);
}

void setDirection(dawn::proto::statements::Direction* directionProto,
                  const sir::Direction* direction) {
  directionProto->set_name(direction->Name);
  setLocation(directionProto->mutable_loc(), direction->Loc);
}

void setOffset(dawn::proto::statements::Offset* offsetProto, const sir::Offset* offset) {
  offsetProto->set_name(offset->Name);
  setLocation(offsetProto->mutable_loc(), offset->Loc);
}

void setField(dawn::proto::statements::Field* fieldProto, const sir::Field* field) {
  fieldProto->set_name(field->Name);
  fieldProto->set_is_temporary(field->IsTemporary);
  for(const auto& initializedDimension : field->fieldDimensions) {
    fieldProto->add_field_dimensions(initializedDimension);
  }
  setLocation(fieldProto->mutable_loc(), field->Loc);
  proto::statements::Field_LocationType protoLocationType;
  switch(field->locationType) {
  case dawn::ast::Expr::LocationType::Cells:
    protoLocationType = proto::statements::Field_LocationType_Cell;
    break;
  case dawn::ast::Expr::LocationType::Edges:
    protoLocationType = proto::statements::Field_LocationType_Edge;
    break;
  case dawn::ast::Expr::LocationType::Vertices:
    protoLocationType = proto::statements::Field_LocationType_Vertex;
    break;
  default:
    dawn_unreachable("unknown location type");
  }
  fieldProto->set_location_type(protoLocationType);
}

ProtoStmtBuilder::ProtoStmtBuilder(dawn::proto::statements::Stmt* stmtProto,
                                   dawn::ast::StmtData::DataType dataType)
    : dataType_(dataType) {
  currentStmtProto_.push(stmtProto);
}

ProtoStmtBuilder::ProtoStmtBuilder(dawn::proto::statements::Expr* exprProto,
                                   dawn::ast::StmtData::DataType dataType)
    : dataType_(dataType) {
  currentExprProto_.push(exprProto);
}

dawn::proto::statements::Stmt* ProtoStmtBuilder::getCurrentStmtProto() {
  DAWN_ASSERT(!currentStmtProto_.empty());
  return currentStmtProto_.top();
}

dawn::proto::statements::Expr* ProtoStmtBuilder::getCurrentExprProto() {
  DAWN_ASSERT(!currentExprProto_.empty());
  return currentExprProto_.top();
}

void ProtoStmtBuilder::visit(const std::shared_ptr<BlockStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_block_stmt();

  for(const auto& s : stmt->getStatements()) {
    currentStmtProto_.push(protoStmt->add_statements());
    s->accept(*this);
    currentStmtProto_.pop();
  }

  setStmtData(protoStmt->mutable_data(), *stmt);

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());
  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<ExprStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_expr_stmt();
  currentExprProto_.push(protoStmt->mutable_expr());
  stmt->getExpr()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<ReturnStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_return_stmt();

  currentExprProto_.push(protoStmt->mutable_expr());
  stmt->getExpr()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<VarDeclStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_var_decl_stmt();

  if(stmt->getType().isBuiltinType())
    setBuiltinType(protoStmt->mutable_type()->mutable_builtin_type(),
                   stmt->getType().getBuiltinTypeID());
  else
    protoStmt->mutable_type()->set_name(stmt->getType().getName());
  protoStmt->mutable_type()->set_is_const(stmt->getType().isConst());
  protoStmt->mutable_type()->set_is_volatile(stmt->getType().isVolatile());

  protoStmt->set_name(stmt->getName());
  protoStmt->set_dimension(stmt->getDimension());
  protoStmt->set_op(stmt->getOp());

  for(const auto& expr : stmt->getInitList()) {
    currentExprProto_.push(protoStmt->add_init_list());
    expr->accept(*this);
    currentExprProto_.pop();
  }

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setVarDeclStmtData(protoStmt->mutable_var_decl_stmt_data(), *stmt);

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<VerticalRegionDeclStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_vertical_region_decl_stmt();

  dawn::sir::VerticalRegion* verticalRegion = stmt->getVerticalRegion().get();
  dawn::proto::statements::VerticalRegion* verticalRegionProto =
      protoStmt->mutable_vertical_region();

  // VerticalRegion.Loc
  setLocation(verticalRegionProto->mutable_loc(), verticalRegion->Loc);

  // VerticalRegion.Ast
  setAST(verticalRegionProto->mutable_ast(), verticalRegion->Ast.get());

  // VerticalRegion.VerticalInterval
  setInterval(verticalRegionProto->mutable_interval(), verticalRegion->VerticalInterval.get());

  // VerticalRegion.LoopOrder
  verticalRegionProto->set_loop_order(verticalRegion->LoopOrder ==
                                              dawn::sir::VerticalRegion::LoopOrderKind::Backward
                                          ? dawn::proto::statements::VerticalRegion::Backward
                                          : dawn::proto::statements::VerticalRegion::Forward);

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<StencilCallDeclStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_stencil_call_decl_stmt();

  dawn::ast::StencilCall* stencilCall = stmt->getStencilCall().get();
  dawn::proto::statements::StencilCall* stencilCallProto = protoStmt->mutable_stencil_call();

  // StencilCall.Loc
  setLocation(stencilCallProto->mutable_loc(), stencilCall->Loc);

  // StencilCall.Callee
  stencilCallProto->set_callee(stencilCall->Callee);

  // StencilCall.Args
  for(const auto& argName : stencilCall->Args) {
    stencilCallProto->add_arguments(argName);
  }

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<BoundaryConditionDeclStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_boundary_condition_decl_stmt();
  protoStmt->set_functor(stmt->getFunctor());

  for(const auto& fieldName : stmt->getFields())
    protoStmt->add_fields(fieldName);

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<IfStmt>& stmt) {
  auto protoStmt = getCurrentStmtProto()->mutable_if_stmt();

  currentStmtProto_.push(protoStmt->mutable_cond_part());
  stmt->getCondStmt()->accept(*this);
  currentStmtProto_.pop();

  currentStmtProto_.push(protoStmt->mutable_then_part());
  stmt->getThenStmt()->accept(*this);
  currentStmtProto_.pop();

  if(stmt->getElseStmt()) {
    currentStmtProto_.push(protoStmt->mutable_else_part());
    stmt->getElseStmt()->accept(*this);
    currentStmtProto_.pop();
  }

  setLocation(protoStmt->mutable_loc(), stmt->getSourceLocation());

  setStmtData(protoStmt->mutable_data(), *stmt);

  protoStmt->set_id(stmt->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<UnaryOperator>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_unary_operator();
  protoExpr->set_op(expr->getOp());

  currentExprProto_.push(protoExpr->mutable_operand());
  expr->getOperand()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<BinaryOperator>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_binary_operator();
  protoExpr->set_op(expr->getOp());

  currentExprProto_.push(protoExpr->mutable_left());
  expr->getLeft()->accept(*this);
  currentExprProto_.pop();

  currentExprProto_.push(protoExpr->mutable_right());
  expr->getRight()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<AssignmentExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_assignment_expr();
  protoExpr->set_op(expr->getOp());

  currentExprProto_.push(protoExpr->mutable_left());
  expr->getLeft()->accept(*this);
  currentExprProto_.pop();

  currentExprProto_.push(protoExpr->mutable_right());
  expr->getRight()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<TernaryOperator>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_ternary_operator();

  currentExprProto_.push(protoExpr->mutable_cond());
  expr->getCondition()->accept(*this);
  currentExprProto_.pop();

  currentExprProto_.push(protoExpr->mutable_left());
  expr->getLeft()->accept(*this);
  currentExprProto_.pop();

  currentExprProto_.push(protoExpr->mutable_right());
  expr->getRight()->accept(*this);
  currentExprProto_.pop();

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<FunCallExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_fun_call_expr();
  protoExpr->set_callee(expr->getCallee());

  for(const auto& arg : expr->getArguments()) {
    currentExprProto_.push(protoExpr->add_arguments());
    arg->accept(*this);
    currentExprProto_.pop();
  }

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<StencilFunCallExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_stencil_fun_call_expr();
  protoExpr->set_callee(expr->getCallee());

  for(const auto& arg : expr->getArguments()) {
    currentExprProto_.push(protoExpr->add_arguments());
    arg->accept(*this);
    currentExprProto_.pop();
  }

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<StencilFunArgExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_stencil_fun_arg_expr();

  protoExpr->mutable_dimension()->set_direction(
      expr->getDimension() == -1
          ? dawn::proto::statements::Dimension::Invalid
          : static_cast<dawn::proto::statements::Dimension_Direction>(expr->getDimension()));
  protoExpr->set_offset(expr->getOffset());
  protoExpr->set_argument_index(expr->getArgumentIndex());

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<VarAccessExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_var_access_expr();

  protoExpr->set_name(expr->getName());
  protoExpr->set_is_external(expr->isExternal());

  if(expr->isArrayAccess()) {
    currentExprProto_.push(protoExpr->mutable_index());
    expr->getIndex()->accept(*this);
    currentExprProto_.pop();
  }

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  if(dataType_ == StmtData::IIR_DATA_TYPE)
    setAccessExprData(protoExpr->mutable_data(), expr->getData<iir::IIRAccessExprData>());
  else
    protoExpr->mutable_data();
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<FieldAccessExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_field_access_expr();

  protoExpr->set_name(expr->getName());

  auto const& offset = expr->getOffset();
  ast::offset_dispatch(
      offset.horizontalOffset(),
      [&](ast::CartesianOffset const& hOffset) {
        protoExpr->mutable_cartesian_offset()->set_i_offset(hOffset.offsetI());
        protoExpr->mutable_cartesian_offset()->set_j_offset(hOffset.offsetJ());
      },
      [&](ast::UnstructuredOffset const& hOffset) {
        protoExpr->mutable_unstructured_offset()->set_has_offset(hOffset.hasOffset());
      },
      [&] { protoExpr->mutable_zero_offset(); });
  protoExpr->set_vertical_offset(offset.verticalOffset());

  for(int argOffset : expr->getArgumentOffset())
    protoExpr->add_argument_offset(argOffset);

  for(int argMap : expr->getArgumentMap())
    protoExpr->add_argument_map(argMap);

  protoExpr->set_negate_offset(expr->negateOffset());

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  if(dataType_ == StmtData::IIR_DATA_TYPE)
    setAccessExprData(protoExpr->mutable_data(), expr->getData<iir::IIRAccessExprData>());
  else
    protoExpr->mutable_data();
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<LiteralAccessExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_literal_access_expr();

  protoExpr->set_value(expr->getValue());
  setBuiltinType(protoExpr->mutable_type(), expr->getBuiltinType());

  setLocation(protoExpr->mutable_loc(), expr->getSourceLocation());
  if(dataType_ == StmtData::IIR_DATA_TYPE)
    setAccessExprData(protoExpr->mutable_data(), expr->getData<iir::IIRAccessExprData>());
  else
    protoExpr->mutable_data();
  protoExpr->set_id(expr->getID());
}

void ProtoStmtBuilder::visit(const std::shared_ptr<ReductionOverNeighborExpr>& expr) {
  auto protoExpr = getCurrentExprProto()->mutable_reduction_over_neighbor_expr();

  protoExpr->set_op(expr->getOp());

  currentExprProto_.push(protoExpr->mutable_rhs());
  expr->getRhs()->accept(*this);
  currentExprProto_.pop();

  currentExprProto_.push(protoExpr->mutable_init());
  expr->getInit()->accept(*this);
  currentExprProto_.pop();
}

void setAST(proto::statements::AST* astProto, const AST* ast) {
  // Dynamically determine data type
  auto dataType = ast->getRoot()->getDataType();
  ProtoStmtBuilder builder(astProto->mutable_root(), dataType);
  ast->accept(builder);
}

static mlir::iir::BuiltinType convertToBuiltinType(const BuiltinTypeID& id) {
  mlir::iir::BuiltinType builtinType = mlir::iir::BuiltinType::Invalid;

  switch(id) {
  case BuiltinTypeID::Invalid:
    builtinType = mlir::iir::BuiltinType::Invalid;
    break;
  case BuiltinTypeID::Auto:
    builtinType = mlir::iir::BuiltinType::Auto;
    break;
  case BuiltinTypeID::Boolean:
    builtinType = mlir::iir::BuiltinType::Boolean;
    break;
  case BuiltinTypeID::Integer:
    builtinType = mlir::iir::BuiltinType::Integer;
    break;
  case BuiltinTypeID::Float:
    builtinType = mlir::iir::BuiltinType::Float;
    break;
  default:
    break;
  }

  return builtinType;
}

MLIRStmtBuilder::MLIRStmtBuilder(mlir::OpBuilder& builder) { currentBuilder_.push(builder); }

mlir::OpBuilder MLIRStmtBuilder::getCurrentBuilder() const {
  DAWN_ASSERT(!currentBuilder_.empty());
  return currentBuilder_.top();
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::BlockStmt>& stmt) {
  auto builder = getCurrentBuilder();

  auto blockStmtOp =
      builder.create<mlir::iir::BlockStatementOp>(builder.getUnknownLoc(), stmt->getID());
  for(const auto& s : stmt->getStatements()) {
    mlir::OpBuilder stmtBuilder(blockStmtOp.getBody().getTerminator());
    currentBuilder_.push(stmtBuilder);
    s->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::ExprStmt>& stmt) {
  auto builder = getCurrentBuilder();
  auto exprStatementOp =
      builder.create<mlir::iir::ExprStatementOp>(builder.getUnknownLoc(), stmt->getID());

  mlir::OpBuilder exprBuilder(exprStatementOp.getBody().getTerminator());
  currentBuilder_.push(exprBuilder);
  stmt->getExpr()->accept(*this);
  currentBuilder_.pop();

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::VerticalRegionDeclStmt>& stmt) {
  llvm::errs() << "VerticalRegionDeclStmt\n";
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::ReturnStmt>& stmt) {
  llvm::errs() << "ReturnStmt\n";
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::VarDeclStmt>& stmt) {
  auto builder = getCurrentBuilder();
  auto varDeclStatement =
      builder.create<mlir::iir::VarDeclStatementOp>(builder.getUnknownLoc(), stmt->getID());
  llvm::SmallVector<mlir::NamedAttribute, 3> typeAttrs;

  if(stmt->getType().isBuiltinType()) {
    mlir::Identifier id = builder.getIdentifier("builtinType");
    mlir::Attribute builtinTypeAttr = builder.getI64IntegerAttr(
        static_cast<int64_t>(convertToBuiltinType(stmt->getType().getBuiltinTypeID())));
    typeAttrs.push_back({id, builtinTypeAttr});
  } else {
    mlir::Identifier name = builder.getIdentifier("name");

    mlir::Attribute nameAttr = builder.getStringAttr(stmt->getType().getName());
    typeAttrs.push_back({name, nameAttr});
  }

  mlir::Identifier isConst = builder.getIdentifier("isConst");
  mlir::Attribute constAttr = builder.getBoolAttr(stmt->getType().isConst());
  typeAttrs.push_back({isConst, constAttr});

  mlir::Identifier isVolatile = builder.getIdentifier("isVolatile");
  mlir::Attribute volatileAttr = builder.getBoolAttr(stmt->getType().isVolatile());
  typeAttrs.push_back({isVolatile, volatileAttr});

  varDeclStatement.setAttr("type", builder.getDictionaryAttr(typeAttrs));

  // FIXME: This is ugly but it works
  auto iirOp = builder.getBlock()->getParentOp()->getParentOfType<mlir::iir::IIROp>();
  assert(iirOp && "expected root IIR operation");

  auto accessIDToNameAttr = iirOp.getAccessIDToName();
  assert(accessIDToNameAttr.hasValue() && "expected 'accessIDToName' attribute");
  auto accessIDToName = accessIDToNameAttr.getValue();

  auto accessData = stmt->getData<iir::VarDeclStmtData>();
  auto accessIDData = accessData.AccessID;
  assert(accessIDData.has_value() && "expected accessID to be valid");
  auto accessID = makeMLIRIdentifier(accessIDData.value(), builder);

  auto varName = accessIDToName.get(accessID);
  assert(varName && "expected access name to be present in accessIDToName");

  varDeclStatement.setAttr("name", varName);
  varDeclStatement.setAttr("dimension", builder.getI64IntegerAttr(stmt->getDimension()));
  varDeclStatement.setAttr(
      "op", builder.getI64IntegerAttr(static_cast<int64_t>(mlir::iir::AssignmentOperator::Equal)));

  for(const auto& expr : stmt->getInitList()) {
    mlir::OpBuilder initListBuilder(varDeclStatement.getBody().getTerminator());
    currentBuilder_.push(initListBuilder);
    expr->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::StencilCallDeclStmt>& stmt) {
  auto builder = getCurrentBuilder();

  auto stencilCallDeclStatement =
      builder.create<mlir::iir::StencilCallDeclStatementOp>(builder.getUnknownLoc(), stmt->getID());

  dawn::ast::StencilCall* stencilCall = stmt->getStencilCall().get();
  mlir::OpBuilder stencilCallBuilder(stencilCallDeclStatement.getBody().getTerminator());

  llvm::SmallVector<llvm::StringRef, 3> args;
  for(const auto& argName : stencilCall->Args) {
    args.push_back(argName);
  }

  stencilCallBuilder.create<mlir::iir::StencilCallOp>(stencilCallBuilder.getUnknownLoc(),
                                                      stencilCall->Callee, args);

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::BoundaryConditionDeclStmt>& stmt) {
  llvm::errs() << "BoundaryConditionDeclStmt\n";
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::IfStmt>& stmt) {
  auto builder = getCurrentBuilder();

  auto ifOp = builder.create<mlir::iir::IfOp>(builder.getUnknownLoc(), stmt->getID());

  mlir::OpBuilder condBuilder(ifOp.getCond().getTerminator());
  currentBuilder_.push(condBuilder);
  stmt->getCondStmt()->accept(*this);
  currentBuilder_.pop();

  mlir::OpBuilder thenBuilder(ifOp.getThen().getTerminator());
  currentBuilder_.push(thenBuilder);
  stmt->getThenStmt()->accept(*this);
  currentBuilder_.pop();

  if(stmt->getElseStmt()) {
    mlir::OpBuilder elseBuilder(ifOp.getElse().getTerminator());
    currentBuilder_.push(elseBuilder);
    stmt->getElseStmt()->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::UnaryOperator>& expr) {
  auto builder = getCurrentBuilder();

  mlir::iir::UnaryOperator op;
  std::string opStr = expr->getOp();
  if(opStr == "-") {
    op = mlir::iir::UnaryOperator::UnaryMinus;
  } else {
    dawn_unreachable((std::string("Unsupported unary operator ") + opStr).c_str());
  }

  auto unaryOperator =
      builder.create<mlir::iir::UnaryOperatorOp>(builder.getUnknownLoc(), expr->getID(), op);

  mlir::OpBuilder operandBuilder(unaryOperator.getBody().getTerminator());
  currentBuilder_.push(operandBuilder);
  expr->getOperand()->accept(*this);
  currentBuilder_.pop();

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::BinaryOperator>& expr) {
  auto builder = getCurrentBuilder();

  mlir::iir::BinaryOperator op;
  std::string opStr = expr->getOp();
  if(opStr == "+") {
    op = mlir::iir::BinaryOperator::Plus;
  } else if(opStr == "-") {
    op = mlir::iir::BinaryOperator::Minus;
  } else if(opStr == "*") {
    op = mlir::iir::BinaryOperator::Times;
  } else if(opStr == "/") {
    op = mlir::iir::BinaryOperator::Divide;
  } else if(opStr == "<") {
    op = mlir::iir::BinaryOperator::Lt;
  } else if(opStr == ">") {
    op = mlir::iir::BinaryOperator::Gt;
  } else if(opStr == ">=") {
    op = mlir::iir::BinaryOperator::Ge;
  } else if(opStr == "<=") {
    op = mlir::iir::BinaryOperator::Le;
  } else if(opStr == "==") {
    op = mlir::iir::BinaryOperator::IsEqual;
  } else if(opStr == "&&") {
    op = mlir::iir::BinaryOperator::And;
  } else if(opStr == "||") {
    op = mlir::iir::BinaryOperator::Or;
  } else {
    dawn_unreachable((std::string("Unsupported binary operator ") + opStr).c_str());
  }

  auto binaryOperatorOp =
      builder.create<mlir::iir::BinaryOperatorOp>(builder.getUnknownLoc(), expr->getID(), op);

  mlir::OpBuilder leftBuilder(binaryOperatorOp.getLeft().getTerminator());
  currentBuilder_.push(leftBuilder);
  expr->getLeft()->accept(*this);
  currentBuilder_.pop();

  mlir::OpBuilder rightBuilder(binaryOperatorOp.getRight().getTerminator());
  currentBuilder_.push(rightBuilder);
  expr->getRight()->accept(*this);
  currentBuilder_.pop();

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::AssignmentExpr>& expr) {
  auto builder = getCurrentBuilder();

  mlir::iir::AssignmentOperator op;
  std::string opStr = expr->getOp();
  if(opStr == "=") {
    op = mlir::iir::AssignmentOperator::Equal;
  } else if(opStr == "+=") {
    op = mlir::iir::AssignmentOperator::PlusEqual;
  } else if(opStr == "-=") {
    op = mlir::iir::AssignmentOperator::MinusEqual;
  } else if(opStr == "*=") {
    op = mlir::iir::AssignmentOperator::TimesEqual;
  } else if(opStr == "/=") {
    op = mlir::iir::AssignmentOperator::DivideEqual;
  } else {
    dawn_unreachable((std::string("Unsupported operation ") + opStr).c_str());
  }

  auto assignmentExprOp =
      builder.create<mlir::iir::AssignmentExprOp>(builder.getUnknownLoc(), expr->getID(), op);

  mlir::OpBuilder leftBuilder(assignmentExprOp.getLeft().getTerminator());
  currentBuilder_.push(leftBuilder);
  expr->getLeft()->accept(*this);
  currentBuilder_.pop();

  mlir::OpBuilder rightBuilder(assignmentExprOp.getRight().getTerminator());
  currentBuilder_.push(rightBuilder);
  expr->getRight()->accept(*this);
  currentBuilder_.pop();

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::TernaryOperator>& expr) {
  auto builder = getCurrentBuilder();

  auto ternaryOperatorOp =
      builder.create<mlir::iir::TernaryOperatorOp>(builder.getUnknownLoc(), expr->getID());

  mlir::OpBuilder condBuilder(ternaryOperatorOp.getCond().getTerminator());
  currentBuilder_.push(condBuilder);
  expr->getCondition()->accept(*this);
  currentBuilder_.pop();

  mlir::OpBuilder leftBuilder(ternaryOperatorOp.getLeft().getTerminator());
  currentBuilder_.push(leftBuilder);
  expr->getLeft()->accept(*this);
  currentBuilder_.pop();

  mlir::OpBuilder rightBuilder(ternaryOperatorOp.getRight().getTerminator());
  currentBuilder_.push(rightBuilder);
  expr->getRight()->accept(*this);
  currentBuilder_.pop();

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::FunCallExpr>& expr) {
  auto builder = getCurrentBuilder();

  auto funCallExprOp = builder.create<mlir::iir::FunCallOp>(builder.getUnknownLoc(), expr->getID(),
                                                            expr->getCallee());
  for(const auto& arg : expr->getArguments()) {
    mlir::OpBuilder argumentBuilder(funCallExprOp.getBody().getTerminator());
    currentBuilder_.push(argumentBuilder);
    arg->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::StencilFunCallExpr>& expr) {
  auto builder = getCurrentBuilder();

  auto stencilFunCallOp = builder.create<mlir::iir::StencilFunCallOp>(
      builder.getUnknownLoc(), expr->getID(), expr->getCallee());
  for(const auto& arg : expr->getArguments()) {
    mlir::OpBuilder argumentBuilder(stencilFunCallOp.getBody().getTerminator());
    currentBuilder_.push(argumentBuilder);
    arg->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::StencilFunArgExpr>& expr) {
  auto builder = getCurrentBuilder();

  mlir::iir::Dimension dimension = mlir::iir::Dimension::Invalid;
  switch(expr->getDimension()) {
  case 0:
    dimension = mlir::iir::Dimension::I;
    break;
  case 1:
    dimension = mlir::iir::Dimension::J;
    break;
  case 2:
    dimension = mlir::iir::Dimension::K;
    break;
  default:
    break;
  }
  builder.create<mlir::iir::StencilFunArgExprOp>(builder.getUnknownLoc(), expr->getID(),
                                                 expr->getArgumentIndex(), expr->getOffset(),
                                                 dimension);

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::VarAccessExpr>& expr) {
  auto builder = getCurrentBuilder();

  // FIXME: This is ugly but it works
  auto iirOp = builder.getBlock()->getParentOp()->getParentOfType<mlir::iir::IIROp>();
  assert(iirOp && "expected root IIR operation");

  auto accessIDToNameAttr = iirOp.getAccessIDToName();
  assert(accessIDToNameAttr.hasValue() && "expected 'accessIDToName' attribute");
  auto accessIDToName = accessIDToNameAttr.getValue();

  auto accessData = expr->getData<iir::IIRAccessExprData>();
  auto accessIDData = accessData.AccessID;
  assert(accessIDData.has_value() && "expected accessID to be valid");
  auto accessID = makeMLIRIdentifier(accessIDData.value(), builder);

  auto varName = accessIDToName.get(accessID);
  assert(varName && "expected access name to be present in accessIDToName");

  auto varAccessExprOp = builder.create<mlir::iir::VarAccessExprOp>(
      builder.getUnknownLoc(), expr->getID(), varName.cast<mlir::StringAttr>().getValue(),
      expr->isExternal());

  if(expr->isArrayAccess()) {
    mlir::OpBuilder indexBuilder(varAccessExprOp.getIndex().getTerminator());
    currentBuilder_.push(indexBuilder);
    expr->getIndex()->accept(*this);
    currentBuilder_.pop();
  }

  // TODO Handle location?
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::FieldAccessExpr>& expr) {
  auto builder = getCurrentBuilder();

  // FIXME: This is ugly but it works
  auto iirOp = builder.getBlock()->getParentOp()->getParentOfType<mlir::iir::IIROp>();
  assert(iirOp && "expected root IIR operation");

  auto accessIDToNameAttr = iirOp.getAccessIDToName();
  assert(accessIDToNameAttr.hasValue() && "expected 'accessIDToName' attribute");
  auto accessIDToName = accessIDToNameAttr.getValue();

  auto accessData = expr->getData<iir::IIRAccessExprData>();
  auto accessIDData = accessData.AccessID;
  assert(accessIDData.has_value() && "expected accessID to be valid");
  auto accessID = makeMLIRIdentifier(accessIDData.value(), builder);

  auto fieldName = accessIDToName.get(accessID);
  assert(fieldName && "expected access name to be present in accessIDToName");

  auto const& hoffset =
      ast::offset_cast<CartesianOffset const&>(expr->getOffset().horizontalOffset());
  auto const& voffset = expr->getOffset().verticalOffset();
  mlir::SmallVector<int64_t, 3> offsetAttr = {hoffset.offsetI(), hoffset.offsetJ(), voffset};
  auto argumentMap = expr->getArgumentMap();
  mlir::SmallVector<int64_t, 3> argumentMapAttr = {argumentMap[0], argumentMap[1], argumentMap[2]};
  auto argumentOffset = expr->getArgumentOffset();
  mlir::SmallVector<int64_t, 3> argumentOffsetAttr = {argumentOffset[0], argumentOffset[1],
                                                      argumentOffset[2]};

  builder.create<mlir::iir::FieldAccessExprOp>(
      builder.getUnknownLoc(), expr->getID(), fieldName.cast<mlir::StringAttr>().getValue(),
      offsetAttr, argumentMapAttr, argumentOffsetAttr, expr->negateOffset());
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::LiteralAccessExpr>& expr) {
  auto builder = getCurrentBuilder();

  builder.create<mlir::iir::LiteralAccessExprOp>(builder.getUnknownLoc(), expr->getID(),
                                                 expr->getValue(),
                                                 convertToBuiltinType(expr->getBuiltinType()));
}

void MLIRStmtBuilder::visit(const std::shared_ptr<ast::ReductionOverNeighborExpr>& expr) {
  llvm_unreachable("ReductionOverNeighborExpr is unsupported");
}

//===------------------------------------------------------------------------------------------===//
// Deserialization
//===------------------------------------------------------------------------------------------===//

std::shared_ptr<sir::Field> makeField(const proto::statements::Field& fieldProto) {
  auto field = std::make_shared<sir::Field>(fieldProto.name(), makeLocation(fieldProto));
  field->IsTemporary = fieldProto.is_temporary();
  if(!fieldProto.field_dimensions().empty()) {
    auto throwException = [&fieldProto](const char* member) {
      throw std::runtime_error(
          format("Field::%s (loc %s) exceeds 3 dimensions", member, makeLocation(fieldProto)));
    };
    if(fieldProto.field_dimensions().size() > 3)
      throwException("field_dimensions");

    std::copy(fieldProto.field_dimensions().begin(), fieldProto.field_dimensions().end(),
              field->fieldDimensions.begin());
  }
  switch(fieldProto.location_type()) {
  case proto::statements::Field_LocationType_Cell:
    field->locationType = ast::Expr::LocationType::Cells;
    break;
  case proto::statements::Field_LocationType_Edge:
    field->locationType = ast::Expr::LocationType::Edges;
    break;
  case proto::statements::Field_LocationType_Vertex:
    field->locationType = ast::Expr::LocationType::Vertices;
    break;
  default:
    dawn_unreachable("unknown location type");
  }
  return field;
}

BuiltinTypeID makeBuiltinTypeID(const proto::statements::BuiltinType& builtinTypeProto) {
  switch(builtinTypeProto.type_id()) {
  case proto::statements::BuiltinType_TypeID_Invalid:
    return BuiltinTypeID::Invalid;
  case proto::statements::BuiltinType_TypeID_Auto:
    return BuiltinTypeID::Auto;
  case proto::statements::BuiltinType_TypeID_Boolean:
    return BuiltinTypeID::Boolean;
  case proto::statements::BuiltinType_TypeID_Integer:
    return BuiltinTypeID::Integer;
  case proto::statements::BuiltinType_TypeID_Float:
    return BuiltinTypeID::Float;
  default:
    return BuiltinTypeID::Invalid;
  }
  return BuiltinTypeID::Invalid;
}

BuiltinTypeID makeBuiltinTypeID(const mlir::iir::BuiltinType& builtinType) {
  switch(builtinType) {
  case mlir::iir::BuiltinType::Invalid:
    return BuiltinTypeID::Invalid;
  case mlir::iir::BuiltinType::Auto:
    return BuiltinTypeID::Auto;
  case mlir::iir::BuiltinType::Boolean:
    return BuiltinTypeID::Boolean;
  case mlir::iir::BuiltinType::Integer:
    return BuiltinTypeID::Integer;
  case mlir::iir::BuiltinType::Float:
    return BuiltinTypeID::Float;
  default:
    return BuiltinTypeID::Invalid;
  }
}

std::shared_ptr<sir::Direction> makeDirection(const proto::statements::Direction& directionProto) {
  return std::make_shared<sir::Direction>(directionProto.name(), makeLocation(directionProto));
}

std::shared_ptr<sir::Offset> makeOffset(const proto::statements::Offset& offsetProto) {
  return std::make_shared<sir::Offset>(offsetProto.name(), makeLocation(offsetProto));
}

std::shared_ptr<sir::Interval> makeInterval(const proto::statements::Interval& intervalProto) {
  int lowerLevel = -1, upperLevel = -1, lowerOffset = -1, upperOffset = -1;

  if(intervalProto.LowerLevel_case() == proto::statements::Interval::kSpecialLowerLevel)
    lowerLevel = intervalProto.special_lower_level() ==
                         proto::statements::Interval_SpecialLevel::Interval_SpecialLevel_Start
                     ? sir::Interval::Start
                     : sir::Interval::End;
  else
    lowerLevel = intervalProto.lower_level();

  if(intervalProto.UpperLevel_case() == proto::statements::Interval::kSpecialUpperLevel)
    upperLevel = intervalProto.special_upper_level() ==
                         proto::statements::Interval_SpecialLevel::Interval_SpecialLevel_Start
                     ? sir::Interval::Start
                     : sir::Interval::End;
  else
    upperLevel = intervalProto.upper_level();

  lowerOffset = intervalProto.lower_offset();
  upperOffset = intervalProto.upper_offset();
  return std::make_shared<sir::Interval>(lowerLevel, upperLevel, lowerOffset, upperOffset);
}

std::shared_ptr<Expr> makeExpr(const proto::statements::Expr& expressionProto,
                               ast::StmtData::DataType dataType) {
  switch(expressionProto.expr_case()) {
  case proto::statements::Expr::kUnaryOperator: {
    const auto& exprProto = expressionProto.unary_operator();
    auto expr = std::make_shared<UnaryOperator>(makeExpr(exprProto.operand(), dataType),
                                                exprProto.op(), makeLocation(exprProto));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kBinaryOperator: {
    const auto& exprProto = expressionProto.binary_operator();
    auto expr = std::make_shared<BinaryOperator>(
        makeExpr(exprProto.left(), dataType), exprProto.op(), makeExpr(exprProto.right(), dataType),
        makeLocation(exprProto));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kAssignmentExpr: {
    const auto& exprProto = expressionProto.assignment_expr();
    auto expr = std::make_shared<AssignmentExpr>(makeExpr(exprProto.left(), dataType),
                                                 makeExpr(exprProto.right(), dataType),
                                                 exprProto.op(), makeLocation(exprProto));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kTernaryOperator: {
    const auto& exprProto = expressionProto.ternary_operator();
    auto expr = std::make_shared<TernaryOperator>(
        makeExpr(exprProto.cond(), dataType), makeExpr(exprProto.left(), dataType),
        makeExpr(exprProto.right(), dataType), makeLocation(exprProto));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kFunCallExpr: {
    const auto& exprProto = expressionProto.fun_call_expr();
    auto expr = std::make_shared<FunCallExpr>(exprProto.callee(), makeLocation(exprProto));
    for(const auto& argProto : exprProto.arguments())
      expr->getArguments().emplace_back(makeExpr(argProto, dataType));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kStencilFunCallExpr: {
    const auto& exprProto = expressionProto.stencil_fun_call_expr();
    auto expr = std::make_shared<StencilFunCallExpr>(exprProto.callee(), makeLocation(exprProto));
    for(const auto& argProto : exprProto.arguments())
      expr->getArguments().emplace_back(makeExpr(argProto, dataType));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kStencilFunArgExpr: {
    const auto& exprProto = expressionProto.stencil_fun_arg_expr();
    int direction = -1, offset = 0, argumentIndex = -1; // default values

    if(exprProto.has_dimension()) {
      switch(exprProto.dimension().direction()) {
      case proto::statements::Dimension_Direction_I:
        direction = 0;
        break;
      case proto::statements::Dimension_Direction_J:
        direction = 1;
        break;
      case proto::statements::Dimension_Direction_K:
        direction = 2;
        break;
      case proto::statements::Dimension_Direction_Invalid:
      default:
        direction = -1;
        break;
      }
    }
    offset = exprProto.offset();
    argumentIndex = exprProto.argument_index();
    auto expr = std::make_shared<StencilFunArgExpr>(direction, offset, argumentIndex,
                                                    makeLocation(exprProto));
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kVarAccessExpr: {
    const auto& exprProto = expressionProto.var_access_expr();
    auto expr = std::make_shared<VarAccessExpr>(
        exprProto.name(), exprProto.has_index() ? makeExpr(exprProto.index(), dataType) : nullptr,
        makeLocation(exprProto));
    expr->setIsExternal(exprProto.is_external());
    if(dataType == StmtData::IIR_DATA_TYPE)
      fillAccessExprDataFromProto(expr->getData<iir::IIRAccessExprData>(), exprProto.data());
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kFieldAccessExpr: {

    using ProtoFieldAccessExpr = dawn::proto::statements::FieldAccessExpr;
    const auto& exprProto = expressionProto.field_access_expr();
    auto name = exprProto.name();
    auto negateOffset = exprProto.negate_offset();

    auto throwException = [&exprProto](const char* member) {
      throw std::runtime_error(format("FieldAccessExpr::%s (loc %s) exceeds 3 dimensions", member,
                                      makeLocation(exprProto)));
    };

    ast::Offsets offset;
    switch(exprProto.horizontal_offset_case()) {
    case ProtoFieldAccessExpr::kCartesianOffset: {
      auto const& hOffset = exprProto.cartesian_offset();
      offset = ast::Offsets{ast::cartesian, hOffset.i_offset(), hOffset.j_offset(),
                            exprProto.vertical_offset()};
      break;
    }
    case ProtoFieldAccessExpr::kUnstructuredOffset: {
      auto const& hOffset = exprProto.unstructured_offset();
      offset = ast::Offsets{ast::unstructured, hOffset.has_offset(), exprProto.vertical_offset()};
      break;
    }
    case ProtoFieldAccessExpr::kZeroOffset:
      offset = ast::Offsets{ast::HorizontalOffset{}, exprProto.vertical_offset()};
      break;
    default:
      dawn_unreachable("unknown offset");
    }

    Array3i argumentOffset{{0, 0, 0}};
    if(!exprProto.argument_offset().empty()) {
      if(exprProto.argument_offset().size() > 3)
        throwException("argument_offset");

      std::copy(exprProto.argument_offset().begin(), exprProto.argument_offset().end(),
                argumentOffset.begin());
    }

    Array3i argumentMap{{-1, -1, -1}};
    if(!exprProto.argument_map().empty()) {
      if(exprProto.argument_map().size() > 3)
        throwException("argument_map");

      std::copy(exprProto.argument_map().begin(), exprProto.argument_map().end(),
                argumentMap.begin());
    }

    auto expr = std::make_shared<FieldAccessExpr>(name, offset, argumentMap, argumentOffset,
                                                  negateOffset, makeLocation(exprProto));
    if(dataType == StmtData::IIR_DATA_TYPE)
      fillAccessExprDataFromProto(expr->getData<iir::IIRAccessExprData>(), exprProto.data());
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::kLiteralAccessExpr: {
    const auto& exprProto = expressionProto.literal_access_expr();
    auto expr = std::make_shared<LiteralAccessExpr>(
        exprProto.value(), makeBuiltinTypeID(exprProto.type()), makeLocation(exprProto));
    if(dataType == StmtData::IIR_DATA_TYPE)
      fillAccessExprDataFromProto(expr->getData<iir::IIRAccessExprData>(), exprProto.data());
    expr->setID(exprProto.id());
    return expr;
  }
  case proto::statements::Expr::EXPR_NOT_SET:
  default:
    dawn_unreachable("expr not set");
  }
  return nullptr;
}

static std::string assignmentOperatorToString(mlir::iir::AssignmentOperator op) {
  switch(op) {
  case mlir::iir::AssignmentOperator::Equal:
    return {"="};
  case mlir::iir::AssignmentOperator::PlusEqual:
    return {"+="};
  case mlir::iir::AssignmentOperator::MinusEqual:
    return {"-="};
  case mlir::iir::AssignmentOperator::TimesEqual:
    return {"*="};
  case mlir::iir::AssignmentOperator::DivideEqual:
    return {"/="};
  default:
    dawn_unreachable("unsupported MLIR assignment operator");
  }
}

static std::string binaryOperatorToString(mlir::iir::BinaryOperator op) {
  switch(op) {
  case mlir::iir::BinaryOperator::Plus:
    return {"+"};
  case mlir::iir::BinaryOperator::Minus:
    return {"-"};
  case mlir::iir::BinaryOperator::Times:
    return {"*"};
  case mlir::iir::BinaryOperator::Divide:
    return {"/"};
  case mlir::iir::BinaryOperator::Lt:
    return {"<"};
  case mlir::iir::BinaryOperator::Gt:
    return {">"};
  case mlir::iir::BinaryOperator::Le:
    return {"<="};
  case mlir::iir::BinaryOperator::Ge:
    return {">="};
  case mlir::iir::BinaryOperator::IsEqual:
    return {"=="};
  case mlir::iir::BinaryOperator::And:
    return {"&&"};
  case mlir::iir::BinaryOperator::Or:
    return {"||"};
  default:
    dawn_unreachable("unsupported MLIR binary operator");
  }
}

static std::string unaryOperatorToString(mlir::iir::UnaryOperator op) {
  switch(op) {
  case mlir::iir::UnaryOperator::UnaryMinus:
    return {"-"};
    break;
  default:
    dawn_unreachable("unsupported MLIR unary operator");
  }
}

std::shared_ptr<Expr> makeExpr(mlir::Operation& expression, ast::StmtData::DataType dataType) {
  if(auto assignmentExpr = mlir::dyn_cast<mlir::iir::AssignmentExprOp>(expression)) {
    std::string op = assignmentOperatorToString(assignmentExpr.op());

    auto& left = assignmentExpr.getLeft().front();
    auto& right = assignmentExpr.getRight().front();

    auto expr = std::make_shared<AssignmentExpr>(makeExpr(left, dataType),
                                                 makeExpr(right, dataType), op, SourceLocation());
    expr->setID(assignmentExpr.id().getLimitedValue());
    return expr;
  } else if(auto fieldAccessExpr = mlir::dyn_cast<mlir::iir::FieldAccessExprOp>(expression)) {
    auto name = fieldAccessExpr.name();
    auto negateOffset = fieldAccessExpr.negateOffset();

    auto offsetAttr = fieldAccessExpr.offset().getValue();
    Offsets offset(cartesian, offsetAttr[0].cast<mlir::IntegerAttr>().getInt(),
                   offsetAttr[1].cast<mlir::IntegerAttr>().getInt(),
                   offsetAttr[2].cast<mlir::IntegerAttr>().getInt());

    Array3i argumentOffset({0, 0, 0});
    auto argumentOffsetAttr = fieldAccessExpr.argumentOffset().getValue();
    argumentOffset[0] = argumentOffsetAttr[0].cast<mlir::IntegerAttr>().getInt();
    argumentOffset[1] = argumentOffsetAttr[1].cast<mlir::IntegerAttr>().getInt();
    argumentOffset[2] = argumentOffsetAttr[2].cast<mlir::IntegerAttr>().getInt();

    Array3i argumentMap({0, 0, 0});
    auto argumentMapAttr = fieldAccessExpr.argumentMap().getValue();
    argumentMap[0] = argumentMapAttr[0].cast<mlir::IntegerAttr>().getInt();
    argumentMap[1] = argumentMapAttr[1].cast<mlir::IntegerAttr>().getInt();
    argumentMap[2] = argumentMapAttr[2].cast<mlir::IntegerAttr>().getInt();

    auto expr = std::make_shared<FieldAccessExpr>(name, offset, argumentMap, argumentOffset,
                                                  negateOffset, SourceLocation());
    expr->setID(fieldAccessExpr.id().getLimitedValue());
    return expr;
  } else if(auto binaryOperator = mlir::dyn_cast<mlir::iir::BinaryOperatorOp>(expression)) {
    std::string op = binaryOperatorToString(binaryOperator.op());

    auto& left = binaryOperator.getLeft().front();
    auto& right = binaryOperator.getRight().front();

    auto expr = std::make_shared<BinaryOperator>(makeExpr(left, dataType), op,
                                                 makeExpr(right, dataType), SourceLocation());
    expr->setID(binaryOperator.id().getLimitedValue());
    return expr;
  } else if(auto varAccessExpr = mlir::dyn_cast<mlir::iir::VarAccessExprOp>(expression)) {
    // exprProto.has_index() ? makeExpr(exprProto.index()) : nullptr
    auto& index = varAccessExpr.getIndex().front();
    std::shared_ptr<Expr> indexExpr = nullptr;
    if(!mlir::isa<mlir::iir::VarAccessExprEndOp>(index)) {
      indexExpr = makeExpr(index, dataType);
    }
    auto expr = std::make_shared<VarAccessExpr>(varAccessExpr.name(), indexExpr, SourceLocation());
    expr->setIsExternal(varAccessExpr.isExternal());
    expr->setID(varAccessExpr.id().getLimitedValue());
    return expr;
  } else if(auto literalAccessExpr = mlir::dyn_cast<mlir::iir::LiteralAccessExprOp>(expression)) {
    auto expr = std::make_shared<LiteralAccessExpr>(
        literalAccessExpr.value(), makeBuiltinTypeID(literalAccessExpr.builtinType()),
        SourceLocation());
    expr->setID(literalAccessExpr.id().getLimitedValue());
    return expr;
  } else if(auto ternaryOperator = mlir::dyn_cast<mlir::iir::TernaryOperatorOp>(expression)) {
    auto& cond = ternaryOperator.getCond().front();
    auto& left = ternaryOperator.getLeft().front();
    auto& right = ternaryOperator.getRight().front();

    auto expr =
        std::make_shared<TernaryOperator>(makeExpr(cond, dataType), makeExpr(left, dataType),
                                          makeExpr(right, dataType), SourceLocation());
    expr->setID(ternaryOperator.id().getLimitedValue());
    return expr;
  } else if(auto unaryOperator = mlir::dyn_cast<mlir::iir::UnaryOperatorOp>(expression)) {
    std::string op = unaryOperatorToString(unaryOperator.op());

    auto expr = std::make_shared<UnaryOperator>(makeExpr(unaryOperator.getBody().front(), dataType),
                                                op, SourceLocation());
    return expr;
  } else if(auto functionCallOp = mlir::dyn_cast<mlir::iir::FunCallOp>(expression)) {
    auto expr = std::make_shared<FunCallExpr>(functionCallOp.callee(), SourceLocation());
    for(auto& arg : functionCallOp.getBody()) {
      if(!mlir::isa<mlir::iir::FunCallEndOp>(arg)) {
        expr->getArguments().emplace_back(makeExpr(arg, dataType));
      }
    }
    expr->setID(functionCallOp.id().getLimitedValue());
    return expr;
  } else if(auto stencilFunctionCallOp = mlir::dyn_cast<mlir::iir::StencilFunCallOp>(expression)) {
    auto expr =
        std::make_shared<StencilFunCallExpr>(stencilFunctionCallOp.callee(), SourceLocation());
    for(auto& arg : stencilFunctionCallOp.getBody()) {
      if(!mlir::isa<mlir::iir::StencilFunCallEndOp>(arg)) {
        expr->getArguments().emplace_back(makeExpr(arg, dataType));
      }
    }
    expr->setID(stencilFunctionCallOp.id().getLimitedValue());
    return expr;
  } else if(auto stencilFunArgExprOp = mlir::dyn_cast<mlir::iir::StencilFunArgExprOp>(expression)) {
    int direction = -1, offset = 0, argumentIndex = -1; // default values
    auto dimensionAttr = stencilFunArgExprOp.getAttr("dimension");
    if(dimensionAttr) {
      auto dimension =
          *mlir::iir::symbolizeDimension(dimensionAttr.cast<mlir::IntegerAttr>().getInt());
      switch(dimension) {
      case mlir::iir::Dimension::I:
        direction = 0;
        break;
      case mlir::iir::Dimension::J:
        direction = 1;
        break;
      case mlir::iir::Dimension::K:
        direction = 2;
        break;
      case mlir::iir::Dimension::Invalid:
        direction = -1;
        break;
      }
    }

    auto offsetAttr = stencilFunArgExprOp.getAttr("offset");
    DAWN_ASSERT_MSG(offsetAttr, "stencilFunArgExprOp requires an 'offset' attribute");
    offset = offsetAttr.cast<mlir::IntegerAttr>().getInt();

    auto indexAttr = stencilFunArgExprOp.getAttr("index");
    DAWN_ASSERT_MSG(indexAttr, "stencilFunArgExprOp requires an 'index' attribute");
    argumentIndex = indexAttr.cast<mlir::IntegerAttr>().getInt();

    auto expr =
        std::make_shared<StencilFunArgExpr>(direction, offset, argumentIndex, SourceLocation());
    expr->setID(stencilFunArgExprOp.id().getLimitedValue());
    return expr;
  } else {
    expression.dump();
    dawn_unreachable("unsupported MLIR expression type");
  }
}

std::shared_ptr<Stmt> makeStmt(const proto::statements::Stmt& statementProto,
                               ast::StmtData::DataType dataType) {
  switch(statementProto.stmt_case()) {
  case proto::statements::Stmt::kBlockStmt: {
    const auto& stmtProto = statementProto.block_stmt();
    auto stmt =
        std::make_shared<BlockStmt>(makeData(dataType, stmtProto.data()), makeLocation(stmtProto));

    for(const auto& s : stmtProto.statements())
      stmt->push_back(makeStmt(s, dataType));
    stmt->setID(stmtProto.id());

    return stmt;
  }
  case proto::statements::Stmt::kExprStmt: {
    const auto& stmtProto = statementProto.expr_stmt();
    auto stmt =
        std::make_shared<ExprStmt>(makeData(dataType, stmtProto.data()),
                                   makeExpr(stmtProto.expr(), dataType), makeLocation(stmtProto));
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kReturnStmt: {
    const auto& stmtProto = statementProto.return_stmt();
    auto stmt =
        std::make_shared<ReturnStmt>(makeData(dataType, stmtProto.data()),
                                     makeExpr(stmtProto.expr(), dataType), makeLocation(stmtProto));
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kVarDeclStmt: {
    const auto& stmtProto = statementProto.var_decl_stmt();

    std::vector<std::shared_ptr<Expr>> initList;
    for(const auto& e : stmtProto.init_list())
      initList.emplace_back(makeExpr(e, dataType));

    const proto::statements::Type& typeProto = stmtProto.type();
    CVQualifier cvQual = CVQualifier::Invalid;
    if(typeProto.is_const())
      cvQual |= CVQualifier::Const;
    if(typeProto.is_volatile())
      cvQual |= CVQualifier::Volatile;
    Type type = typeProto.name().empty() ? Type(makeBuiltinTypeID(typeProto.builtin_type()), cvQual)
                                         : Type(typeProto.name(), cvQual);

    auto stmt = std::make_shared<VarDeclStmt>(
        makeVarDeclStmtData(dataType, stmtProto.data(), stmtProto.var_decl_stmt_data()), type,
        stmtProto.name(), stmtProto.dimension(), stmtProto.op().c_str(), initList,
        makeLocation(stmtProto));
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kStencilCallDeclStmt: {
    auto metaloc = makeLocation(statementProto.stencil_call_decl_stmt());
    const auto& stmtProto = statementProto.stencil_call_decl_stmt();
    auto loc = makeLocation(stmtProto.stencil_call());
    std::shared_ptr<ast::StencilCall> call =
        std::make_shared<ast::StencilCall>(stmtProto.stencil_call().callee(), loc);
    for(const auto& argName : stmtProto.stencil_call().arguments()) {
      call->Args.push_back(argName);
    }
    auto stmt =
        std::make_shared<StencilCallDeclStmt>(makeData(dataType, stmtProto.data()), call, metaloc);
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kVerticalRegionDeclStmt: {
    const auto& stmtProto = statementProto.vertical_region_decl_stmt();
    auto loc = makeLocation(stmtProto.vertical_region());
    std::shared_ptr<sir::Interval> interval = makeInterval(stmtProto.vertical_region().interval());
    sir::VerticalRegion::LoopOrderKind looporder;
    switch(stmtProto.vertical_region().loop_order()) {
    case proto::statements::VerticalRegion_LoopOrder_Forward:
      looporder = sir::VerticalRegion::LoopOrderKind::Forward;
      break;
    case proto::statements::VerticalRegion_LoopOrder_Backward:
      looporder = sir::VerticalRegion::LoopOrderKind::Backward;
      break;
    default:
      dawn_unreachable("no looporder specified");
    }
    auto ast = makeAST(stmtProto.vertical_region().ast(), dataType);
    std::shared_ptr<sir::VerticalRegion> verticalRegion =
        std::make_shared<sir::VerticalRegion>(ast, interval, looporder, loc);
    auto stmt = std::make_shared<VerticalRegionDeclStmt>(makeData(dataType, stmtProto.data()),
                                                         verticalRegion, loc);
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kBoundaryConditionDeclStmt: {
    const auto& stmtProto = statementProto.boundary_condition_decl_stmt();
    auto stmt = std::make_shared<BoundaryConditionDeclStmt>(
        makeData(dataType, stmtProto.data()), stmtProto.functor(), makeLocation(stmtProto));
    for(const auto& fieldName : stmtProto.fields())
      stmt->getFields().emplace_back(fieldName);
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kIfStmt: {
    const auto& stmtProto = statementProto.if_stmt();
    auto stmt = std::make_shared<IfStmt>(
        makeData(dataType, stmtProto.data()), makeStmt(stmtProto.cond_part(), dataType),
        makeStmt(stmtProto.then_part(), dataType),
        stmtProto.has_else_part() ? makeStmt(stmtProto.else_part(), dataType) : nullptr,
        makeLocation(stmtProto));
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::STMT_NOT_SET:
  default:
    dawn_unreachable("stmt not set");
  }
  return nullptr;
}

std::shared_ptr<Stmt> makeStmt(mlir::Operation& statement, ast::StmtData::DataType dataType) {
  // TODO Create statements

  if(auto exprStmt = mlir::dyn_cast<mlir::iir::ExprStatementOp>(statement)) {
    auto& expression = exprStmt.getBody().front();
    auto stmt = std::make_shared<ExprStmt>(makeData(dataType), makeExpr(expression, dataType),
                                           SourceLocation());
    stmt->setID(exprStmt.id().getLimitedValue());
    return stmt;
  } else if(auto varDeclStmt = mlir::dyn_cast<mlir::iir::VarDeclStatementOp>(statement)) {
    std::vector<std::shared_ptr<Expr>> initList;
    for(auto& op : varDeclStmt.getBody()) {
      if(!mlir::isa<mlir::iir::VarDeclStatementEndOp>(op))
        initList.emplace_back(makeExpr(op, dataType));
    }

    auto typeAttr = varDeclStmt.getAttrOfType<mlir::DictionaryAttr>("type");
    DAWN_ASSERT_MSG(typeAttr, "varDeclStmt needs a 'type' attribute");

    CVQualifier cvQual = CVQualifier::Invalid;
    if(typeAttr.get("isConst"))
      cvQual |= CVQualifier::Const;
    if(typeAttr.get("isVolatile"))
      cvQual |= CVQualifier::Volatile;

    Type type;
    mlir::Attribute name = typeAttr.get("name");
    if(name)
      type = Type(name.cast<mlir::StringAttr>().getValue(), cvQual);
    else {
      mlir::Attribute builtinTypeAttr = typeAttr.get("builtinType");
      DAWN_ASSERT_MSG(
          builtinTypeAttr,
          "varDeclStmt type needs a 'builtinType' attribute if it has no 'name' attribute");
      auto builtinType =
          mlir::iir::symbolizeBuiltinType(builtinTypeAttr.cast<mlir::IntegerAttr>().getInt());
      DAWN_ASSERT_MSG(builtinType.hasValue(), "unknown builtin type");
      type = Type(makeBuiltinTypeID(*builtinType), cvQual);
    }

    auto nameAttr = varDeclStmt.getAttrOfType<mlir::StringAttr>("name");
    DAWN_ASSERT_MSG(nameAttr, "varDeclStmt needs a 'name' attribute");
    auto dimensionAttr = varDeclStmt.getAttrOfType<mlir::IntegerAttr>("dimension");
    DAWN_ASSERT_MSG(dimensionAttr, "varDeclStmt needs a 'dimensionAttr' attribute");
    auto opAttr = varDeclStmt.getAttrOfType<mlir::IntegerAttr>("op");
    DAWN_ASSERT_MSG(opAttr, "varDeclStmt needs an 'op' attribute");

    auto op = mlir::iir::symbolizeAssignmentOperator(opAttr.getInt());
    DAWN_ASSERT_MSG(op.hasValue(), "unknown assignment operator");

    auto stmt = std::make_shared<VarDeclStmt>(
        makeData(dataType), type, nameAttr.getValue(), dimensionAttr.getInt(),
        assignmentOperatorToString(*op).c_str(), initList, SourceLocation());
    stmt->setID(varDeclStmt.id().getLimitedValue());
    return stmt;
  } else if(auto ifOp = mlir::dyn_cast<mlir::iir::IfOp>(statement)) {
    auto& condOp = ifOp.getCond().front();
    auto& thenOp = ifOp.getThen().front();
    auto& elseOp = ifOp.getElse().front();

    std::shared_ptr<Stmt> elseStmt = nullptr;
    if(!mlir::isa<mlir::iir::IfEndOp>(elseOp)) {
      elseStmt = makeStmt(elseOp, dataType);
    }

    auto stmt = std::make_shared<IfStmt>(makeData(dataType), makeStmt(condOp, dataType),
                                         makeStmt(thenOp, dataType), elseStmt, SourceLocation());
    stmt->setID(ifOp.id().getLimitedValue());

    return stmt;
  } else if(auto blockStmtOp = mlir::dyn_cast<mlir::iir::BlockStatementOp>(statement)) {
    auto stmt = std::make_shared<BlockStmt>(makeData(dataType), SourceLocation());

    for(auto& s : blockStmtOp.getBody()) {
      if(!mlir::isa<mlir::iir::BlockStatementEndOp>(s))
        stmt->push_back(makeStmt(s, dataType));
    }
    stmt->setID(blockStmtOp.id().getLimitedValue());

    return stmt;
  } else if(auto stencilCallDeclStmt =
                mlir::dyn_cast<mlir::iir::StencilCallDeclStatementOp>(statement)) {
    auto stencilCall = mlir::cast<mlir::iir::StencilCallOp>(stencilCallDeclStmt.getBody().front());
    std::shared_ptr<ast::StencilCall> call =
        std::make_shared<ast::StencilCall>(stencilCall.callee().str(), SourceLocation());

    for(auto& argNameAttr : stencilCall.arguments()) {
      auto argName = argNameAttr.cast<mlir::StringAttr>().getValue().str();
      call->Args.push_back(argName);
    }

    auto stmt = std::make_shared<StencilCallDeclStmt>(makeData(dataType), call, SourceLocation());
    stmt->setID(stencilCallDeclStmt.id().getLimitedValue());
    return stmt;
  } else {
    statement.dump();
    dawn_unreachable("unsupported MLIR statement type");
  }

  /*switch(statementProto.stmt_case()) {
  case proto::statements::Stmt::kReturnStmt: {
    const auto& stmtProto = statementProto.return_stmt();
    auto stmt = std::make_shared<ReturnStmt>(makeExpr(stmtProto.expr()), makeLocation(stmtProto));
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kVerticalRegionDeclStmt: {
    const auto& stmtProto = statementProto.vertical_region_decl_stmt();
    auto loc = makeLocation(stmtProto.vertical_region());
    std::shared_ptr<sir::Interval> interval = makeInterval(stmtProto.vertical_region().interval());
    sir::VerticalRegion::LoopOrderKind looporder;
    switch(stmtProto.vertical_region().loop_order()) {
    case proto::statements::VerticalRegion_LoopOrder_Forward:
      looporder = sir::VerticalRegion::LK_Forward;
      break;
    case proto::statements::VerticalRegion_LoopOrder_Backward:
      looporder = sir::VerticalRegion::LK_Backward;
      break;
    default:
      dawn_unreachable("no looporder specified");
    }
    auto ast = makeAST(stmtProto.vertical_region().ast());
    std::shared_ptr<sir::VerticalRegion> verticalRegion =
        std::make_shared<sir::VerticalRegion>(ast, interval, looporder, loc);
    auto stmt = std::make_shared<VerticalRegionDeclStmt>(verticalRegion, loc);
    stmt->setID(stmtProto.id());
    return stmt;
  }
  case proto::statements::Stmt::kBoundaryConditionDeclStmt: {
    const auto& stmtProto = statementProto.boundary_condition_decl_stmt();
    auto stmt =
        std::make_shared<BoundaryConditionDeclStmt>(stmtProto.functor(), makeLocation(stmtProto));
    for(const auto& fieldName : stmtProto.fields())
      stmt->getFields().emplace_back(fieldName);
    stmt->setID(stmtProto.id());
    return stmt;
  }*/
}

std::shared_ptr<AST> makeAST(const dawn::proto::statements::AST& astProto,
                             ast::StmtData::DataType dataType) {
  auto root = dyn_pointer_cast<BlockStmt>(makeStmt(astProto.root(), dataType));
  if(!root)
    throw std::runtime_error("root statement of AST is not a 'BlockStmt'");
  auto ast = std::make_shared<AST>(root);
  return ast;
}
